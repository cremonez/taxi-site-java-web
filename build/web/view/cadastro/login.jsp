<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Login</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<div align="center">
    <a href="create">Criar meu cadastro</a>
</div>  
<br>

<form action="login" method="post">

<table class="table tabela" align="center">
    <tr><td>
        
            
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Login</b></h1></div></td></td>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/email.png" width="32">
            <b>Email:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="taxi_email" class="form-control input_text" placeholder="Insira seu email cadastrado">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/password.png" width="32">
            <b>Senha:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="password" name="taxi_senha" class="form-control input_text" placeholder="Insira sua senha cadastrada">
        </td>
    </tr>
    
        
    
    
    
</table>
            
            
    </td></tr>
</table>



<div align="center">
    <a href="pesquisa.jsp">
        <input type="submit" value="Entrar" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>

</form>

<br><br>



<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>