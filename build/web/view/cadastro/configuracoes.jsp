<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Configurações</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<div align="center">
    <div style="background-image: url(${pageContext.servletContext.contextPath}/includes/media/perfil/${sessao.fotografia}); width: 300px; height: 300px; background-size:cover; border-radius:100%; line-height: 450px; box-shadow:#000000 0px 0px 10px -2px;">
        <a href="editImageProfile">
            Editar minha imagem de perfil
        </a>
    </div>
</div>
        

        


<c:if test="${sessao.valorkm == -1}">
    <br>
    <div align="center">
        <a href="upgrade">
            <input type="submit" value="Me tornar um motorista" class="btn btn-warning call_driver" style="height: 64px">
        </a>
    </div>
    <br>
    <div align="center">
        <a href="edit">
            <input type="submit" value="Editar meu cadastro" class="btn btn-warning call_driver" style="height: 64px">
        </a>
    </div>
    <br>
    <div align="center">
        <form action="delete" method="post">
            <a href="#">
                <input type="submit" value="Deletar meu cadastro" class="btn btn-warning call_driver" style="height: 64px">
            </a>
        </form>
    </div>
</c:if>
<c:if test="${sessao.valorkm >= 0}">
    <br> 
    <div align="center">
        <img width="450" src="${pageContext.servletContext.contextPath}/includes/media/carro/${veiculo.fotografia}" style="background-size:100%; border-radius:10px; line-height: 450px; box-shadow:#000000 0px 0px 10px -2px;">
            <br>
            <a href="editImageCar">
                Editar a imagem do meu carro
            </a>
        </div>
    </div>
    <br>
    <div align="center">
        <a href="editDriver">
            <input type="submit" value="Editar meu cadastro" class="btn btn-warning call_driver" style="height: 64px">
        </a>
    </div>
    <br>
    <div align="center">
        <form action="delete" method="post">
            <a href="#">
                <input type="submit" value="Deletar meu cadastro" class="btn btn-warning call_driver" style="height: 64px">
            </a>
        </form>
    </div>
</c:if>


<br>
<br>

<br>





<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>