<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Estatísticas</title>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>

</head>

<body>

    
<%@include file="/view/includes/top_menu.jsp"%>  



<br>
<table width="450" align="center" class="table tabela" border="0">
    <tr>
        <td class="label1" align="center">
            Maior valor por km rodado por Marca
        </td>
    </tr>
    <tr>
        <td align="center">   
            <!---->
            <table width="100%" class="table-hover" border="0">
            <c:set var="i" value="0"/>
            <c:forEach items="${resultados}" var="resultado">
                <tr height="140">
                    <td align="center"><img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/taxi_car.png" width="100"></td>
                    <td class="label4" width="150" align="center">${resultado.texto}</td>
                    <td align="center" class="label4">
                        <b>R$ ${resultado.valorkm}</b>
                    </td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </table>
            <!---->
        </td>
    </tr>
    
</table>



<br>
<table width="450" align="center" class="table tabela" border="0">
    <tr>
        <td class="label1" align="center">
            Maior valor por km rodado por Modelo
        </td>
    </tr>
    <tr>
        <td align="center">   
            <!---->
            <table width="100%" class="table-hover" border="0">
            <c:set var="i" value="0"/>
            <c:forEach items="${resultados}" var="resultado">
                <tr height="140">
                    <td align="center"><img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/taxi_car.png" width="100"></td>
                    <td class="label4" width="150" align="center">${resultado.texto}</td>
                    <td align="center" class="label4">
                        <b>R$ ${resultado.valorkm}</b>
                    </td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </table>
            <!---->
        </td>
    </tr>
    
</table>
<br><br>

${ok}


<%@include file="/view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>