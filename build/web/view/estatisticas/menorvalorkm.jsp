<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Estatísticas</title>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>

</head>

<body>

    
<%@include file="/view/includes/top_menu.jsp"%>  

<br>
<table width="450" align="center" class="table tabela" border="0">
    <tr>
        <td class="label1" align="center" colspan="2">
            Corrida com o menor valor por Km rodado
        </td>
    </tr>
    <tr>
        <td class="label1" align="center" colspan="2">
            <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corrida.id}" class="box_ticket">
                Corrida<br>
                Ticket #UEL${corrida.id}<br>
                <span style="font-size: 16px;">Valor Km rodado: R$${corrida.valorkm}</span><br>
            </a>
        </td>
    </tr>
</table>



<br>


    

            <br>




<%@include file="/view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>