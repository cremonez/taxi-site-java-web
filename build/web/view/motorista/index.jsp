<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Todos Motoristas</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<div align="center">
    <a href="pesquisar">
        <input type="button" value="Pesquisar" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>
<br>

<table width="450" align="center" class="table tabela" border="0">
<tr><td align="center" class="label1">Todos os taxistas</td></tr>
<tr><td align="center">

        
<%@include file="../../view/includes/preview_motorista.jsp"%>

</td></tr>
</table>
<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>