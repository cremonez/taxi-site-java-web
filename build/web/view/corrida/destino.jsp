<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Corrida</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<%@include file="../../view/includes/find_taxi2.jsp"%>

<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>



<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>