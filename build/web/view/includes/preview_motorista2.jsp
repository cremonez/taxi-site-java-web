<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table class="table-hover">
<c:set var="i" value="0"/>
<c:forEach items="${motoristas}" var="motorista">
    <c:set var="i" value="${i+1}"/>
    <tr height="150">
        <td>
            <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${motorista.id}">
                <div style="background-image:url(${pageContext.servletContext.contextPath}/includes/media/perfil/${motorista.fotografia}); background-size:cover; width:128px; height: 128px; border-radius:100%"></div>
            </a>
        </td>
        <td width="100%" valign="middle" style="padding-bottom:10px">
            <div><b>${motorista.nome} ${motorista.sobrenome}</b></div>
            <div>Carro: Chevrolet ONIX 2014 Preto</div>
            <br>
            <div>Pontua��o:
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            </div>
            <div><a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${motorista.id}#qualificacoes">Ver indica��es de outros passageiros</a></div>
            
            Rota #<span id="ponto_proximo_${motorista.id}">${pontos_proximos[i].id}</span>
            <input type="submit" id="id_${motorista.id}" onclick="escolherTaxista(this.id)" value="Escolher este taxista" class="btn btn-warning search_taxi">
        </td>
    </tr>


</c:forEach>
</table>
<input type="hidden" value="0" name="id" id="id">
<input type="hidden" value="0" name="ponto_proximo" id="ponto_proximo">
<script>
    function escolherTaxista(id){
        numId = id.split("_")[1];
        document.getElementById("id").value = numId;
        document.getElementById("ponto_proximo").value = document.getElementById("ponto_proximo_"+numId).innerHTML;
    }
</script>
