<tr>
    <td colspan="2" align="center">
        Nenhuma avalia��o registrada ainda!
    </td>
</tr>
<c:forEach items="${resultados}" var="resultado">
    <tr style="margin-bottom: 10px">
        <td>
            <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${resultado.cliente}" target="blank">
                <div class="prof" style="background-image:url(${pageContext.servletContext.contextPath}/includes/media/perfil/${resultado.cliente}.png); width:96px; height: 96px;"></div>
            </a>
        </td>
        <td width="100%">
            <div align="center">
                <c:set var="estrelas" value="${resultado.quantidade}"/>
                    <%  
                        int estrelas = (int)pageContext.getAttribute("estrelas");
                    %>
                    <%for(int i=0; i<estrelas; i++){%>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
                    <% }%>
                    <%for(int i=estrelas; i<5; i++){%>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
                    <% }%>
                <br>
                
            </div>
            <div align="center"><br>
                ${resultado.texto}
            </div>
        </td>
    </tr>
</c:forEach>