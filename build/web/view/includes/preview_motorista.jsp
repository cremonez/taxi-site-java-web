<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table class="table-hover">
<c:forEach items="${motoristas}" var="motorista">
    <tr height="150">
    <td>
        <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${motorista.id}">
            <div style="background-image:url(${pageContext.servletContext.contextPath}/includes/media/perfil/${motorista.fotografia}); background-size:cover; width:128px; height: 128px; border-radius:100%"></div>
        </a>
    </td>
    <td width="100%" valign="middle">
    <div><b>${motorista.nome} ${motorista.sobrenome}</b></div>
    <div>Carro: Chevrolet ONIX 2014 Preto</div>
    <br>
    <div>Pontua��o:
    <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
    <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
    <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
    <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
    <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
    </div>
    <div><a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${motorista.id}#qualificacoes">Ver indica��es de outros passageiros</a></div>
    </td>
    </tr>


</c:forEach>
</table>
