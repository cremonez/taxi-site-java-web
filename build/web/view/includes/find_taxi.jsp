<form action="corrida/destino" method="POST">
    <input type="hidden" value="<%= request.getParameter("id") %>" name="id">
    <table width="450" align="center" class="table tabela pin">
        <tr>
            <td></td>
            <td align="center" class="label1">Onde estou agora?</td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="text" id="latitude" name="latitude_origem" class="form-control lat_lon" placeholder="Insira a sua Latitude"></td>
        </tr>
        <tr>
            <td></td>
            <td align="left"><input type="text" id="longitude" name="longitude_origem" class="form-control lat_lon" placeholder="Insira sua Longitude"></td>
        </tr>
        <tr>
            <td width="100%"></td>
            <td align="left"><input type="submit" value="Localizar TAXI" onclick="enviarDados(event)" class="btn btn-warning search_taxi"> </td>
        </tr>
    </table>
</form>

<script>
    function enviarDados(e){
        if(document.getElementById("latitude").value === "" || document.getElementById("longitude").value === ""){
            alert("Insira o local onde voc� est� primeiro!");
            e.preventDefault();
        }
    }
</script>


<table width="450" align="center" class="table tabela">
    <tr>
        <td align="center" class="label1">
            Marque seu local de origem
        </td>
    </tr>
    <tr>
        <td>
            <div id="my-map" style="width: 100%; height: 400px; background-color: #ECD946; "></div>
        </td>
    </tr>
</table>
