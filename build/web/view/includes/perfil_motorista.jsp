
<table align="center" class="table tabela">
    <tr>
        <td>
            <a href="${pageContext.servletContext.contextPath}/includes/media/perfil/${usuario.fotografia}" target="blank">
                <div class="prof" style="background-image:url(${pageContext.servletContext.contextPath}/includes/media/perfil/${usuario.fotografia}); width:128px; height: 128px;">
                </div>
            </a>
        </td>
        <td width="100%" align="center" style="vertical-align: middle">
            <div class="label1">${usuario.nome} ${usuario.sobrenome}</div>
            <div>(Motorista)</div>
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <br>
            <div style="font-size:11px; color:#777">(Nenhuma quaifica��o ainda!)</div>
        </td>
    </tr>
    <tr><td colspan="2">
            <table class="table table-hover" align="center" width="100%">
            <tr><td><b>Valor KM Rodado:</b> <div class="label1">R$ ${usuario.valorkm}</div><br></td></tr>
            <tr><td><b><div id="veiculo">Ve�culo:</div></b><br>
                    - Marca: ${veiculo.marca}<br>
                    - Modelo: ${veiculo.modelo}<br>
                    - Ano: ${veiculo.ano}<br>
                    - Placa: ${veiculo.placa}<br>
                    - Cor: ${veiculo.cor}<br>
                    - Renavam: ${veiculo.renavam}<br>
                    - Capacidade de Passageiros: ${veiculo.capacidade} <i>(${veiculo.capacidade+1} incluindo o motorista)</i><br>
                    <a href="${pageContext.servletContext.contextPath}/includes/media/carro/${veiculo.fotografia}" target="blank">
                        <img src="${pageContext.servletContext.contextPath}/includes/media/carro/${veiculo.fotografia}" width="100%" style="border-radius:5px">
                    </a>
                    <br><br>
                </td></tr>
            <tr>
                <td>
                    <b>Sexo:</b>
                    <c:if test="${usuario.sexo == true}">
                        Masculino
                    </c:if>
                    <c:if test="${usuario.sexo == false}">
                        Feminino
                    </c:if>
                </td>
            </tr>
            <tr><td><b>Data Nascimento:</b> ${usuario.nascimento}</td></tr>
            <tr><td><b>Telefone:</b> ${usuario.telefone}</td></tr>
            <tr><td><b>CPF:</b> ${usuario.cpf}</td></tr>
        </table>    
    </td></tr>
</table>

<table class="table tabela" align="center">
    <tr><td><div class="label1" align="center">Opini�es dos Passageiros:</div></td></tr>
    <tr><td>
    
            <table class="table table-hover" width="100%" border="0">
                <%@include file="../../view/includes/preview_qualificacoes.jsp"%> 
            </table>
                    
    </td></tr>
</table>