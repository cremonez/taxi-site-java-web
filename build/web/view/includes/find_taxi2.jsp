<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${empty sessao}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/cadastro/login" ></c:redirect>
</c:if>
<c:if test="${param.id != 'null'}">
    <form action="novaCorrida" method="POST" class="navbar-form">
    <input type="hidden" value="2000" name="ponto_proximo" id="ponto_proximo">
</c:if>
<c:if test="${param.id == 'null'}">
    <form action="escolhaMotorista" method="POST" class="navbar-form">
</c:if>

<input type="hidden" value="<%= request.getParameter("id") %>" name="id">
<input type="hidden" value="<%= request.getParameter("latitude_origem") %>" name="latitude_origem">
<input type="hidden" value="<%= request.getParameter("longitude_origem") %>" name="longitude_origem">

<table width="450" align="center" class="table tabela pin">
<tr>
<td></td>
<td align="center" class="label1">Onde quero ir?</td>
</tr>
<tr>
<td></td>
<td align="left"><input type="text" id="latitude" name="latitude_destino" class="form-control lat_lon" placeholder="Insira a Latitude do destino"></td>
</tr>
<tr>
<td></td>
<td align="left"><input type="text" id="longitude" name="longitude_destino" class="form-control lat_lon" placeholder="Insira a Longitude do destino"></td>
</tr>
<tr>
<td width="100%"></td>
<td align="left">
    <input type="submit" value="Localizar TAXI" onclick="enviarDados(event)" class="btn btn-warning search_taxi">
</td>
</tr>
</table>


</form>



<script>
    function enviarDados(e){
        if(document.getElementById("latitude").value === "" || document.getElementById("longitude").value === ""){
            alert("Insira o local onde voc� est� primeiro!");
            e.preventDefault();
        }
    }
</script>


<table width="450" align="center" class="table tabela">
    <tr>
        <td align="center" class="label1">
            Marque seu local de origem
        </td>
    </tr>
    <tr>
        <td>
            <div id="my-map" style="width: 100%; height: 400px; background-color: #ECD946; "></div>
        </td>
    </tr>
</table>