<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Encontre o táxi mais próximo de você</title>

<link rel="stylesheet" type="text/css" href="includes/css/main.css">
<link rel="stylesheet" type="text/css" href="includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>

</head>

<body>

    
<%@include file="/view/includes/top_menu.jsp"%>  

<br>
<%@include file="/view/includes/find_taxi.jsp"%> 

<br>

<table width="450" align="center" class="table tabela" border="0">
<tr><td align="center" class="label1">Encontre o táxi mais próximo de você</td></tr>
<tr>
    <td align="center">
        <img src="${pageContext.servletContext.contextPath}/includes/media/mapa_exemplo.png" width="100%">
    </td>
</tr>



<tr><td align="center" class="label1">Escolha o taxista que você desejar</td></tr>
<tr>
    <td align="center">
        <a href="motorista/todos">
            <img src="${pageContext.servletContext.contextPath}/includes/media/service.png" width="100%">
            <%@include file="/view/includes/preview_motorista.jsp"%>
            Todos os taxistas
        </a>
    </td>
</tr>
<tr><td align="center" class="label1">Compare as estatísticas e dados de corridas já realizadas</td></tr>
<tr>
    <td align="center">
        <a href="estatisticas">
            <div align="center" class="box_statistics">
                Estatísticas Globais
            </div>
        </a>
    </td>
</tr>
</table>


<br>

<%@include file="/view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>