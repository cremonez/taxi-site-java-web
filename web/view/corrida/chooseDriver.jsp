<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Ticket #UEL</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>


<table class="table tabela" align="center">
    <tr>
        <td class="label1" align="center">
            Taxistas com rotas que passam muito pr�ximo deste ponto.
        </td>
    </tr>
    <tr>
        <td>
            <form action="novaCorrida" method="POST">
                <input type="hidden" value="<%= request.getParameter("latitude_origem") %>" name="latitude_origem">
                <input type="hidden" value="<%= request.getParameter("longitude_origem") %>" name="longitude_origem">
                <input type="hidden" value="<%= request.getParameter("latitude_destino") %>" name="latitude_destino">
                <input type="hidden" value="<%= request.getParameter("longitude_destino") %>" name="longitude_destino">
                
                <%@include file="../../view/includes/preview_motorista2.jsp"%>
            </form>
        </td>
    </tr>
    
    <tr>
        <td align="center" class="label1">
             <b>${quantidade}</b> Rotas que passam<br>perto deste ponto
        </td>
    </tr>
    
    <tr>
        <td>
            <img src="../includes/media/mapa_exemplo.png" width="100%">
        </td>
    </tr>
    
    
   
</table>
        
<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>