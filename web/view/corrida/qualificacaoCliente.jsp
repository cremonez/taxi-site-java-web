<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Qualificar corrida</title> 
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>
<form action="#" method="POST">
<table class="table tabela" align="center" border="0">
    <tr>
        <td class="label1" align="center">
            Como voc� descreveria a corrida?
        </td>
    </tr>
    <tr>
        <td>
            <input type="hidden" name="id" value="<%= request.getParameter("id")%>">
            <textarea name="qualificacao" rows="4" cols="50" class="form-control rate_corrida_text"></textarea>
        </td>
    </tr>
    
    <tr>
        <td>
            <input type="radio" name="estrelas" value="1"> 1 Estrela<br>
            <input type="radio" name="estrelas" value="2"> 2 Estrela<br>
            <input type="radio" name="estrelas" value="3"> 3 Estrela<br>
            <input type="radio" name="estrelas" value="4"> 4 Estrela<br>
            <input type="radio" name="estrelas" value="5"> 5 Estrela
        </td>
    </tr>
    
    <tr>
        <td>
            <input type="submit" value="Qualificar motorista" class="btn btn-warning rate_corrida">
        </td>
    </tr>
    
</table>
</form>

<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>
