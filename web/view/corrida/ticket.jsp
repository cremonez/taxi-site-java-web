<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Ticket #UEL${corrida.id}</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<table class="table tabela" align="center">
    <tr>
        <td align="center" class="label1">
            <div class="box_ticket">Ticket #UEL${corrida.id}</div>
        </td>
    </tr>
    <tr>
        <td>
            <b>Motorista:</b> <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${corrida.taxista}">Entrar no perfil</a><br>
            <b>Passageiro:</b> <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${corrida.cliente}">Entrar no perfil</a><br>
            <b>Veiculo:</b> <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${corrida.taxista}#veiculo">Visualizar especifica��es</a><br>
            <b>Origem:</b> lat:${corrida.localizacao_A_latitude} lon:${corrida.localizacao_A_longitude}<br>
            <b>Destino:</b> lat:${corrida.localizacao_B_latitude} lon:${corrida.localizacao_B_longitude}<br>
            <b>Distancia Total:</b> ${corrida.distancia} KM<br>
            <b>Valor KM do Motorista:</b> R$ ${corrida.valorkm}<br>
            <b>Pre�o Total:</b> R$ ${corrida.distancia * corrida.valorkm}<br>
            <br>
            <b>Qualificacao do motorista:</b>
            <c:set var="estrelas_motorista" value="${corrida.estrelas_motorista}"/>
            <c:set var="estrelas_cliente" value="${corrida.estrelas_cliente}"/>
            <%  
                int estrelas_motorista = (int)pageContext.getAttribute("estrelas_motorista");
                int estrelas_cliente = (int)pageContext.getAttribute("estrelas_cliente");
            %>
            <%for(int i=0; i< estrelas_motorista; i++){%>
                <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <% }%>
            <%for(int i=estrelas_motorista; i<5; i++){%>
                <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <% }%>
            <br>
            <i>O motorista ainda n�o avaliou esta corrida!</i>
            <br>
            <b>Qualificacao do passageiro:</b> 
            <%for(int i=0; i<estrelas_cliente; i++){%>
                <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_true.png" width="16">
            <% }%>
            <%for(int i=estrelas_cliente; i<5; i++){%>
                <img src="${pageContext.servletContext.contextPath}/includes/media/rating/star_false.png" width="16">
            <% }%>
            <br>
            <c:if test="${corrida.qualificacao_cliente == null}">
                <i>O passageiro ainda n�o avaliou esta corrida!</i><br>
            </c:if>
            <c:if test="${corrida.qualificacao_cliente != null}">
                <i>${corrida.qualificacao_cliente}</i><br>
            </c:if>
            
        </td>
    </tr>
    <tr>
        <td>
            <div id="my-map" style="width: 100%; height: 400px; background-color: #ECD946; "></div>
            <div id="status" align="center">
                <b><i>Calculando rota... Aguarde!</i></b><br>
                <img src="${pageContext.servletContext.contextPath}/includes/media/loading.gif" width="100">
                <input type="hidden" id="tragetoria_info" value="${corrida.id}">
            </div>
        </td>
    </tr>
</table>

<br>

<%@include file="../../../view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/map.js"></script>

</body>
</html>