<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Sobre n�s</title>
<link rel="stylesheet" type="text/css" href="../../includes/css/main.css">
<link rel="stylesheet" type="text/css" href="../../includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="../../includes/bootstrap/css/bootstrap.css">

    
</head>

<body>
    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<table width="450" align="center" class="table tabela">
    <tr>
        <td align="center">

            <div class="label1">Sobre o <b><i>TAXI</i></b></div>
            <div align="left" style="margin: 16px">
                <b><i>TAXI</i></b> � o site onde voc� pode encontrar o T�xi mais pr�ximo de voc�.
                Voc� tem a possibilidade de escolher o motorista de sua preferencia pr�-vizualizando o perfil do taxista anteriormente. Veja por exemplo seu carro, modelo, ano, qualifica��es de outros passageiros com suas opini�es e notas para o servi�o prestado pelo taxista.
            </div>
        </td>
    </tr>
</table>

<br>
    
<%@include file="../../view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/map.js"></script>

</body>
</html>