<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${corridasSolicitadas}" var="corridaSolicitada">
    <c:if test="${corridaSolicitada.status == -1}">
        
        <table align="center" border="0" width="450" class="table tabela">
            <tr>
                <td align="center" colspan="5" class="label1">Atualiza��o da corrida:<br>Ticket <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corridaSolicitada.id}">#UEL${corridaSolicitada.id}</a></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td align="center" class="label2">Pedido realizado</td>
                <td align="center" class="label2">Aguardando aceita��o do motorista</td>
                <td align="center" class="label2">Motorista recusou a corrida</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td align="center" class="bar_left_red"></td>
                <td align="center" class="bar_middle_red"><img src="includes/media/status/circle_red.png" width="18"></td>
                <td align="center" class="bar_middle_red"><img src="includes/media/status/circle_red.png" width="18"></td>
                <td align="center" class="bar_middle_red"><img src="includes/media/status/circle_red.png" width="18"></td>
                <td align="center" class="bar_right_red"></td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="label2"><i>O TAXISTA RECUSOU SUA CORRIDA - Procure outro <a href="${pageContext.servletContext.contextPath}/corrida">mais pr�ximo de voc�</a> ou escolha um <a href="#">outro qualquer</a></i></td>
            </tr>
        </table>
    </c:if>
    <c:if test="${corridaSolicitada.status == 0}">
        
        <table align="center" border="0" width="450" class="table tabela">
            <tr>
                <td align="center" colspan="5" class="label1">Atualiza��o da corrida:<br>Ticket <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corridaSolicitada.id}">#UEL${corridaSolicitada.id}</a></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td align="center" class="label2">Pedido realizado</td>
                <td align="center" class="label2">Aguardando aceita��o do motorista</td>
                <td align="center" class="label2">Motorista aceitou a corrida</td>
                <td align="center" class="label2" >Qualifica��o</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td align="center" class="bar_left_active"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active_inactive"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_inactive"><img src="includes/media/status/circle_inactive.png" width="18"></td>
                <td align="center" class="bar_middle_inactive"><img src="includes/media/status/circle_inactive.png" width="18"></td>
                <td align="center" class="bar_right_inactive"></td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="label2"><i>VOC� SOLICITOU UMA CORRIDA COM O TAXISTA X - Aguarde ele aceitar sua corrida</i></td>
            </tr>
        </table>
    </c:if>
    <c:if test="${corridaSolicitada.status == 1}">
        
        <table align="center" border="0" width="450" class="table tabela">
            <tr>
                <td align="center" colspan="5" class="label1">Atualiza��o da corrida:<br>Ticket <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corridaSolicitada.id}">#UEL${corridaSolicitada.id}</a></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td align="center" class="label2">Pedido realizado</td>
                <td align="center" class="label2">Aguardando aceita��o do motorista</td>
                <td align="center" class="label2">Motorista aceitou a corrida</td>
                <td align="center" class="label2" >Qualifica��o</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td align="center" class="bar_left_active"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active_inactive"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_inactive"><img src="includes/media/status/circle_inactive.png" width="18"></td>
                <td align="center" class="bar_right_inactive"></td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="label2"><i>O TAXISTA ACEITOU SUA CORRIDA - Quando terminar a corrida n�o esque�a de <a href="${pageContext.servletContext.contextPath}/corrida/qualificar?id=${corridaSolicitada.id}">qualificar</a></i></td>
            </tr>
        </table>
    </c:if>
    <c:if test="${corridaSolicitada.status == 2}">
        
        <table align="center" border="0" width="450" class="table tabela">
            <tr>
                <td align="center" colspan="5" class="label1">Atualiza��o da corrida:<br>Ticket <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corridaSolicitada.id}">#UEL${corridaSolicitada.id}</a></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td align="center" class="label2">Pedido realizado</td>
                <td align="center" class="label2">Aguardando aceita��o do motorista</td>
                <td align="center" class="label2">Motorista aceitou a corrida</td>
                <td align="center" class="label2" >Qualifica��o</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td align="center" class="bar_left_active"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_middle_active"><img src="includes/media/status/circle.png" width="18"></td>
                <td align="center" class="bar_right_active"></td>
            </tr>
            <tr>
                <td align="center" colspan="5" class="label2"><i>Obrigado por usar TAXI - Detalhes desta corrida</i></td>
            </tr>
        </table>
    </c:if>
</c:forEach>









