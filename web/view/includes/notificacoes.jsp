<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${corridasRecebidas}" var="corridaRecebida">
    <c:if test="${corridaRecebida.status == 0}">
        <div class="notificacao">
            Um <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${corridaRecebida.cliente}">cliente</a> solicitou uma corrida <a href="${pageContext.servletContext.contextPath}/corrida/ticket?id=${corridaRecebida.id}">Ticket #UEL${corridaRecebida.id}</a> com voc�:<br>
            <a href="corrida/accept?id=${corridaRecebida.id}">Aceitar</a> | <a href="corrida/reject?id=${corridaRecebida.id}">Recusar</a>
        </div> 
        <br>
    </c:if>
</c:forEach>
