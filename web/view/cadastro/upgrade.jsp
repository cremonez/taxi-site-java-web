<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Tornar-se motorista</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>


<form action="upgrade" method="post">


<table class="table tabela" align="center">
    <tr><td>
        
            
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Ve�culo:</b></h1></div></td></td>
    
    <tr>
        <td align="center" colspan="3">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/money.png" width="32">
            <b>Valor KM Rodado:</b>
        </td>  
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="preco" class="form-control input_text" placeholder="Insira o seu pre�o por KM rodado">
        </td>
    </tr>
    
        
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/passageiros.png" width="32">
            <b>Capacidade de Passageiros:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="capacidade" class="form-control input_text" placeholder="Quantos passageiros seu carro suporta">
        </td>
    </tr>
        
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/marca.png" width="32">
            <b>Marca:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="marca" class="form-control input_text" placeholder="Insira a marca do seu carro">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/modelo.png" width="32">
            <b>Modelo:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="modelo" class="form-control input_text" placeholder="Insira o modelo do seu carro">
        </td>
    </tr>
    
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/renavam.png" width="32">
            <b>Renavam:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="renavam" class="form-control input_text" placeholder="Insira o renavam do seu carro">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/cor.png" width="32">
            <b>Cor:</b>
        </td>
    </tr>
    <tr>
    
        <td colspan="3" align="center">
            <input type="text" name="cor" class="form-control input_text" placeholder="Insira a cor do seu carro">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/placa.png" width="32">
            <b>Placa:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="placa" class="form-control input_text" placeholder="Insira a placa do seu carro">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/ano.png" width="32">
            <b>Ano:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="ano" class="form-control input_text" placeholder="Insira o ano do seu carro">
        </td>
    </tr>
    
    
    
    
</table>
            
            
    </td></tr>
</table>





<div align="center">
    <a href="#.jsp">
        <input type="submit" value="Cadastrar e me tornar motorista" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>

</form>

<br><br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>

