<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Atualizar imagem carro</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  


<br>



<div align="center">
    <img width="450" src="${pageContext.servletContext.contextPath}/includes/media/carro/${veiculo.fotografia}" style="background-size:100%; border-radius:10px; line-height: 450px; box-shadow:#000000 0px 0px 10px -2px;">
</div>


<br>    
    
<form action="updateImageCar" method="post" enctype="multipart/form-data">
<table class="table tabela" align="center">
    <tr><td>
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Cadastro</b></h1></div></td></td>
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/camera.png" width="32">
            <b>Fotografia do veiculo:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="file" name="fotografia" class="form-control input_text">
        </td>
    </tr>
</table>
    
</td></tr></table>
            
           

<div align="center">
    <a href="#">
        <input type="submit" value="Atualizar fotografia" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>
</form>
<br><br><br>


<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>