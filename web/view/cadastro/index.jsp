<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="model.Usuario" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${empty sessao}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/cadastro/login" ></c:redirect>
</c:if>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Meu Cadastro</title>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/status.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>


<%@include file="../../view/includes/top_menu.jsp"%>  



<div align="center">    
<a href="cadastro/logout">Sair</a><br><br>
<b>Área de notificações e atualizações!</b><br><br>

<%@include file="../../view/includes/notificacoes.jsp"%>
<%@include file="../../view/includes/infoCorrida.jsp"%>
<br>


<b>Perfil:</b><br><br>
</div>


<c:if test="${usuario.valorkm == -1}">
    <%@include file="../../view/includes/perfil_passageiro.jsp"%>
</c:if>
<c:if test="${usuario.valorkm >= 0}">
    <%@include file="../../view/includes/perfil_motorista.jsp"%>
</c:if>
    
    


<div align="center">
    <a href="cadastro/perfil?id=${sessao.id}">
        Como os outros veem meu perfil
    </a>
</div>


<div align="center">
    <a href="cadastro/configuracoes">
        Configurações do meu cadastro
    </a>
</div>



<br><br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>