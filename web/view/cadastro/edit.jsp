<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Editar meu cadastro</title> 
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>



<form action="edit" method="post">

<table class="table tabela" align="center">
    <tr><td>
        
            
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Cadastro</b></h1></div></td></td>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/email.png" width="32">
            <b>Email:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="taxi_email" value="${sessao.email}" class="form-control input_text" placeholder="Insira seu email para cadastro">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/password.png" width="32">
            <b>Senha:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="password" name="taxi_senha" class="form-control input_text" placeholder="Digite a nova senha">
        </td>
    </tr>
    
        
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>Nome:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="nome" value="${sessao.nome}" class="form-control input_text" placeholder="Insira seu primeiro nome">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>Sobrenome:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="sobrenome" value="${sessao.sobrenome}" class="form-control input_text" placeholder="Insira seu sobrenome">
        </td>
    </tr>
    
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/sexo.png" width="32">
            <b>Sexo</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <div style="display: inline; margin: 10px">
                <input type="radio" name="sexo" value="true" checked="checked"> Masculino
            </div>
            <div style="display: inline; margin: 10px">
                <input type="radio" name="sexo" value="false"> Feminino
            </div>
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>CPF:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="cpf" value="${sessao.cpf}" class="form-control input_text" placeholder="Insira seu CPF">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/telefone.png" width="32">
            <b>Telefone:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="telefone" value="${sessao.telefone}" class="form-control input_text" placeholder="Insira seu telefone para contato">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/idade.png" width="32">
            <b>Data de Nascimento:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input id="nascimento" type="text" name="nascimento" value="${sessao.nascimento}" class="form-control input_text" placeholder="Insira sua data de nascimento">
        </td>
    </tr>
    
    
   
    
    
</table>
            
            
    </td></tr>
</table>



<div align="center">
    <a href="#">
        <input type="submit" value="Atualizar informações" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>

</form>

<br>



<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>
