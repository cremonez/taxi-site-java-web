<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Pesquisar</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>


<form action="pesquisar" method="post">

<table class="table tabela" align="center">
    <tr><td>
        
            
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Motorista:</b></h1></div></td></td>
    <tr>
        <td align="center" colspan="3">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/money.png" width="32">
            <b>Valor KM Rodado:</b>
        </td>  
    </tr>
    <tr>
        <td align="right">
            <input type="text" name="preco_de" class="form-control range" placeholder="0,00">
        </td>
        <td align="center" width="64" class="vertical_align">
            at�
        </td>
        <td align="left">
            <input type="text" name="preco_ate" class="form-control range" placeholder="Infinito">
        </td>            
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>Nome:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="nome" class="form-control input_text" placeholder="Todos os nomes">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>Sobrenome:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="sobrenome" class="form-control input_text" placeholder="Todos os sobrenomes">
        </td>
    </tr>
    
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/sexo.png" width="32">
            <b>Sexo</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <div style="display: inline; margin: 10px">
                <input type="checkbox" name="masculino" checked="checked"> Masculino
            </div>
            <div style="display: inline; margin: 10px">
                <input type="checkbox" name="feminino" checked="checked"> Feminino
            </div>
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/id.png" width="32">
            <b>CPF:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="cpf" class="form-control input_text" placeholder="Todos os CPF de motorista">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/telefone.png" width="32">
            <b>Telefone:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="telefone" class="form-control input_text" placeholder="Todos os telefones poss�veis">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/idade.png" width="32">
            <b>Idade:</b>
        </td>
    </tr>
    <tr>
        <td align="right">
            <input type="text" name="idade_de" class="form-control range" placeholder="0">
        </td>
        <td align="center" width="64" class="vertical_align">
            at�
        </td>
        <td align="left">
            <input type="text" name="idade_ate" class="form-control range" placeholder="Infinito">
        </td>            
    </tr>
    
    
</table>
            
            
    </td></tr>
</table>







<table class="table tabela" align="center">
    <tr><td>
        
            
            
<table class="table-hover table" align="center" width="100%" border="0">
    <tr><td colspan="3"><div class="label1" align="center"><h1><b>Veiculo:</b></h1></div></td></td>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/passageiros.png" width="32">
            <b>Capacidade de Passageiros:</b>
        </td>
    </tr>
    <tr>
        <td align="right">
            <input type="text" name="idade_de" class="form-control range" placeholder="0">
        </td>
        <td align="center" width="64" class="vertical_align">
            at�
        </td>
        <td align="left">
            <input type="text" name="idade_ate" class="form-control range" placeholder="Infinito">
        </td>  
    </tr>
        
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/marca.png" width="32">
            <b>Marca:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="marca" class="form-control input_text" placeholder="Todas as marcas de carro">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/modelo.png" width="32">
            <b>Modelo:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="modelo" class="form-control input_text" placeholder="Todos os modelos de carro">
        </td>
    </tr>
    
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/renavam.png" width="32">
            <b>Renavam:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="renavam" class="form-control input_text" placeholder="Todos os renavans">
        </td>
    </tr>
    
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/cor.png" width="32">
            <b>Cor:</b>
        </td>
    </tr>
    <tr>
    
        <td colspan="3" align="center">
            <input type="text" name="cor" class="form-control input_text" placeholder="Todas as cores">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/placa.png" width="32">
            <b>Placa:</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <input type="text" name="placa" class="form-control input_text" placeholder="Todas as placas">
        </td>
    </tr>
    
    <tr>
        <td colspan="3" align="center">
            <img src="${pageContext.servletContext.contextPath}/includes/media/icons/ano.png" width="32">
            <b>Ano:</b>
        </td>
    </tr>
    <tr>
        <td align="right">
            <input type="text" name="ano_de" class="form-control range" placeholder="0">
        </td>
        <td align="center" width="64" class="vertical_align">
            at�
        </td>
        <td align="left">
            <input type="text" name="ano_ate" class="form-control range" placeholder="2015">
        </td>            
    </tr>
    
    
</table>
            
            
    </td></tr>
</table>





<div align="center">
    <a href="#">
        <input type="submit" value="Pesquisar" class="btn btn-warning call_driver" style="height: 64px">
    </a>
</div>
<br>
</form>

<br>

<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>

