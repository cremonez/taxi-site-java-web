<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Motorista ${usuario.nome} ${usuario.sobrenome}</title>
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
</head>

<body>

    
<%@include file="../../view/includes/top_menu.jsp"%>  

<br>

<%@include file="../../view/includes/perfil_motorista.jsp"%>

<form action="${pageContext.servletContext.contextPath}/corrida" method="POST">
    <input type="hidden" name="id" value="${usuario.id}">
    <div align="center">
        <input type="submit" value="Chamar este taxista" class="btn btn-warning call_driver">
    </div>
</form>
    
<br>
<br>
<%@include file="../../view/includes/bottom_menu.jsp"%>

</body>
</html>