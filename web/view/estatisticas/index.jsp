<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Estatísticas</title>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>

</head>

<body>

    
<%@include file="/view/includes/top_menu.jsp"%>  

<br>
<table width="450" align="center" class="table tabela" border="0">
    <tr>
        <td class="label1" align="center" colspan="2">
            Estatísticas Globais:
        </td>
    </tr>
    <tr>
        <td class="label1" align="center" colspan="2">
            <div class="box_statistics"></div>
        </td>
    </tr>
    <tr>
        <td class="label1" align="center" colspan="2">
            <!---->
            <table class="table-hover" border="0" width="100%">
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/flagrace.png" width="64">
                    </td>
                    <td width="100%" align="center" valign="middle">
                        <a href="estatisticas/quantidadecorridas" class="label3">
                            <div class="box_label_statistics">Quantidade de corridas já realizadas
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center" valign="middle">
                        <a href="estatisticas/maiorvalorkm" class="label3">
                            <div class="box_label_statistics">Corrida com o maior valor por Km rodado</div>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/menorvalorkm" class="label3 box_label_statistics">
                            Corrida com o menor valor por Km rodado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/mediavalorkm" class="label3 box_label_statistics">
                            Preço médio das corridas realizadas por Km rodado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/distance.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/maiordistancia" class="label3 box_label_statistics">
                            Corrida com maior distância percorrida
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/distance.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/menordistancia" class="label3 box_label_statistics">
                            Corrida com menor distância percorrida
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/distance.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/mediadistancia" class="label3 box_label_statistics">
                            Média de distâncias percorridas nas corridas
                        </a>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/maiorvalortotal" class="label3 box_label_statistics">
                            Corrida com maior valor total cobrado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/menorvalortotal" class="label3 box_label_statistics">
                            Corrida com menor valor total cobrado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/money_wallet.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/mediavalortotal" class="label3 box_label_statistics">
                            Média de valor total cobrado de todas corridas
                        </a>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/taxi_car.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/menorvalorkmveiculomarca" class="label3 box_label_statistics">
                            Menor valor por quilometro rodado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/taxi_car.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/maiorvalorkmveiculomarca" class="label3 box_label_statistics">
                            Maior valor por quilometro rodado
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/taxi_car.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/mediavalorkmveiculomarca" class="label3 box_label_statistics">
                            Média dos valores por quilometros rodados
                        </a>
                    </td>
                </tr>
                
                
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/driver.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10motoristascorridas" class="label3 box_label_statistics">
                            TOP 10 Motoristas que mais fizeram corridas
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/driver.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10motoristasganhos" class="label3 box_label_statistics">
                            TOP 10 Motoristas que mais receberam dinheiro
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/driver.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10motoristasqualificacao" class="label3 box_label_statistics">
                            TOP 10 Motoristas com as melhores qualificações
                        </a>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/passager.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10clientescorridas" class="label3 box_label_statistics">
                            TOP 10 Clientes que mais fizeram corridas
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/passager.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10clientesgastos" class="label3 box_label_statistics">
                            TOP 10 Clientes que mais gastaram dinheiro
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/passager.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10clientesqualificacao" class="label3 box_label_statistics">
                            TOP 10 Clientes com as melhores qualificações
                        </a>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <img src="${pageContext.servletContext.contextPath}/includes/media/estatisticas_icons/satisfaction.png" width="64">
                    </td>
                    <td width="100%" align="center">
                        <a href="estatisticas/top10fidelidade" class="label3 box_label_statistics">
                            TOP 10 Clientes que mais fizeram corridas com um mesmo taxista
                        </a>
                    </td>
                </tr>
            </table>
            <!---->
        </td>
    </tr>
</table>



<br>


    

            <br>




<%@include file="/view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>