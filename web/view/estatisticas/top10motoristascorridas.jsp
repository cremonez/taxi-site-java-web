<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=500, initial-scale=0">
<title>TAXI - Estatísticas</title>

<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/main.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/css/top_menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/includes/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/lib/leaflet.css"/>

</head>

<body>

    
<%@include file="/view/includes/top_menu.jsp"%>  

<br>
<table width="450" align="center" class="table tabela" border="0">
    <tr>
        <td class="label1" align="center">
            TOP 10 Motoristas com mais corridas
        </td>
    </tr>
    <tr>
        <td align="center">   
            <!---->
            <table width="100%" class="table-hover" border="0">
            <c:set var="i" value="0"/>
            <c:forEach items="${resultados}" var="resultado">
                <tr height="140">
                    <td class="label1" width="100" align="center">#${i+1}º</td>
                    <td class="label4" width="150" align="center"><b>${resultado.quantidade}</b> corridas</td>
                    <td align="center">
                        <a href="${pageContext.servletContext.contextPath}/cadastro/perfil?id=${resultado.taxista}">
                            <div class="profile" style="background-image: url(/TAXI/includes/media/perfil/${resultado.taxista}.png);">
                            </div>
                            ${usuarios[i].nome} ${usuarios[i].sobrenome}
                        </a>
                    </td>
                </tr>
                <c:set var="i" value="${i+1}"/>
            </c:forEach>
            </table>
            <!---->
        </td>
    </tr>
    
</table>



<br><br>




<%@include file="/view/includes/bottom_menu.jsp"%>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/lib/leaflet.js" ></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/mapGetPonto.js"></script>

</body>
</html>