function createMarker(ponto, map) {
    var marker = L.marker([ponto.lat, ponto.lng]).addTo(map);
    var message = "<p><strong>Taxi ID: </strong>"+ ponto.carro_id +"</p>";
    message += "<p><strong>Time: </strong>"+ ponto.tempo +"</p>";
    marker.bindPopup(message);
    return marker;
}
function pontoInicial(ponto){
    return L.map('my-map').setView([ponto.lat , ponto.lng], 17);
}

$(document).ready(function() {  
    idTragetoria = document.getElementById("tragetoria_info").value;
    $.get("/TAXI/corrida/tragetoria?id="+idTragetoria, function(rota){
        var map = pontoInicial(rota[0]);
        
        var osmUrl='http://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png';
        var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
        var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});
        map.addLayer(osm);
        /* Get Ponto no mapa
        map.on('click', function(e) {
            alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
        });
        */
        var myMarkers = [];
        var polyline;
    
        for(var i = 0; i<rota.length; i++){
            myMarkers.concat(createMarker(rota[i], map));
        }
        document.getElementById("status").innerHTML = "";
        polyline = L.polyline(rota, {color: 'green', opacity: 0.8}).addTo(map);
    });
});


