package model;

import java.sql.Date;

public class Usuario {
    
    private int id;
    private String email;
    private String senha;
    private String nome;
    private String sobrenome;
    private Date nascimento;
    private String telefone;
    private boolean sexo;
    private String cpf;
    private float valorkm;
    private int veiculo;
    private int status;
    private String fotografia;
    
    // ID
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    // EMAIL
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    // SENHA
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    // NOME
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    // SOBRENOME
    public String getSobrenome() {
        return sobrenome;
    }
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }
    // NASCIMENTO
    public void setNascimento(Date nascimento){
        this.nascimento = nascimento;
    }
    public Date getNascimento(){
        return nascimento;
    }
    // TELEFONE
    public void setTelefone(String telefone){
        this.telefone = telefone;
    }
    public String getTelefone(){
        return telefone;
    }
    // SEXO
    public void setSexo(boolean sexo){
        this.sexo = sexo;
    }
    public boolean getSexo(){
        return sexo;
    }
    // CPF
    public void setCpf(String cpf){
        this.cpf = cpf;
    }
    public String getCpf(){
        return cpf;
    }
    // VALORKM
    public void setValorkm(float valorkm){
        this.valorkm = valorkm;
    }
    public float getValorkm(){
        return valorkm;
    }
    // VEICULO
    public void setVeiculo(int veiculo){
        this.veiculo = veiculo;
    }
    public int getVeiculo(){
        return veiculo;
    }
    // STATUS
    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return status;
    }
    
    // FOTOGRAFIA
    public String getFotografia() {
        return fotografia;
    }
    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }
    
}
