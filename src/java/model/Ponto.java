package model;

import java.sql.Timestamp;

public class Ponto {
    private int id;
    private int carro_id;
    private float lng;
    private float lat;
    private Timestamp tempo;

    public Timestamp getTempo() {
        return tempo;
    }

    public void setTempo(Timestamp tempo) {
        this.tempo = tempo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarro_id() {
        return carro_id;
    }

    public void setCarro_id(int carro_id) {
        this.carro_id = carro_id;
    }

    public float getLongitude() {
        return lng;
    }

    public void setLongitude(float longitude) {
        this.lng = longitude;
    }

    public float getLatitude() {
        return lat;
    }

    public void setLatitude(float latitude) {
        this.lat = latitude;
    }
    
    
    
    
}