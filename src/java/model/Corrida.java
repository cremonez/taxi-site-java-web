package model;

import java.sql.Timestamp;

public class Corrida {
    private int id;
    private int taxista;
    private int cliente;
    private int veiculo;
    private float valorkm;
    private float distancia;
    private Timestamp dataChamada;
    private String localizacao_A_latitude;
    private String localizacao_A_longitude;
    private String localizacao_B_latitude;
    private String localizacao_B_longitude;
    private String qualificacao_motorista;
    private int estrelas_motorista;
    private String qualificacao_cliente;
    private int estrelas_cliente;
    private int status;
    private int pontorota;

    public int getPontorota() {
        return pontorota;
    }

    public void setPontorota(int pontorota) {
        this.pontorota = pontorota;
    }
    
    public Timestamp getDataChamada() {
        return dataChamada;
    }

    public void setDataChamada(Timestamp dataChamada) {
        this.dataChamada = dataChamada;
    }
    
    public String getQualificacao_motorista() {
        return qualificacao_motorista;
    }

    public void setQualificacao_motorista(String qualificacao_motorista) {
        this.qualificacao_motorista = qualificacao_motorista;
    }

    public int getEstrelas_motorista() {
        return estrelas_motorista;
    }

    public void setEstrelas_motorista(int estrelas_motorista) {
        this.estrelas_motorista = estrelas_motorista;
    }

    public String getQualificacao_cliente() {
        return qualificacao_cliente;
    }

    public void setQualificacao_cliente(String qualificacao_cliente) {
        this.qualificacao_cliente = qualificacao_cliente;
    }

    public int getEstrelas_cliente() {
        return estrelas_cliente;
    }

    public void setEstrelas_cliente(int estrelas_cliente) {
        this.estrelas_cliente = estrelas_cliente;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaxista() {
        return taxista;
    }

    public void setTaxista(int taxista) {
        this.taxista = taxista;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(int veiculo) {
        this.veiculo = veiculo;
    }

    public float getValorkm() {
        return valorkm;
    }

    public void setValorkm(float valorkm) {
        this.valorkm = valorkm;
    }

    public float getDistancia() {
        return distancia;
    }

    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }

    public String getLocalizacao_A_latitude() {
        return localizacao_A_latitude;
    }

    public void setLocalizacao_A_latitude(String localizacao_A_latitude) {
        this.localizacao_A_latitude = localizacao_A_latitude;
    }

    public String getLocalizacao_A_longitude() {
        return localizacao_A_longitude;
    }

    public void setLocalizacao_A_longitude(String localizacao_A_longitude) {
        this.localizacao_A_longitude = localizacao_A_longitude;
    }

    public String getLocalizacao_B_latitude() {
        return localizacao_B_latitude;
    }

    public void setLocalizacao_B_latitude(String localizacao_B_latitude) {
        this.localizacao_B_latitude = localizacao_B_latitude;
    }

    public String getLocalizacao_B_longitude() {
        return localizacao_B_longitude;
    }

    public void setLocalizacao_B_longitude(String localizacao_B_longitude) {
        this.localizacao_B_longitude = localizacao_B_longitude;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
