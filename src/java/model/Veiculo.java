package model;


public class Veiculo {
    
    private int id;
    private String marca;
    private String modelo;
    private String cor;
    private int capacidade;
    private String renavam;
    private String placa;
    private int ano;
    private String fotografia;

    
    // ID
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    

    // MARCA
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public String getMarca() {
        return marca;
    }

    // MODELO
    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    // COR
    public String getCor() {
        return cor;
    }
    public void setCor(String cor) {
        this.cor = cor;
    }

    // CAPACIDADE
    public int getCapacidade() {
        return capacidade;
    }
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    // RENAVAM
    public void setRenavam(String renavam){
        this.renavam = renavam;
    }
    public String getRenavam(){
        return renavam;
    }

    // PLACA
    public void setPlaca(String placa){
        this.placa = placa;
    }
    public String getPlaca(){
        return placa;
    }

    // ANO
    public void setAno(int ano){
        this.ano = ano;
    }
    public int getAno(){
        return ano;
    }
    
    // FOTOGRAFIA
    public String getFotografia() {
        return fotografia;
    }
    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }

}
