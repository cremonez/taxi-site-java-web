
package model;

/**
 *
 * @author MacBook
 */
public class Resultado {
    private int quantidade;
    private int taxista;
    private int cliente;
    private String texto;
    private float valorkm;

    public float getValorkm() {
        return valorkm;
    }

    public void setValorkm(float valorkm) {
        this.valorkm = valorkm;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getTaxista() {
        return taxista;
    }

    public void setTaxista(int taxista) {
        this.taxista = taxista;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }
   
    
}
