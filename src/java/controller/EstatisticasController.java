package controller;

import dao.CorridaDAO;
import dao.DAOFactory;
import dao.EstatisticasDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Corrida;
import model.Resultado;
import model.Usuario;

/**
 *
 * @author MacBook
 */
@WebServlet(name = "EstatisticasController",
        urlPatterns = {
            "/estatisticas",
            
            "/estatisticas/quantidadecorridas",
            
            "/estatisticas/maiorvalorkm",
            "/estatisticas/menorvalorkm",
            "/estatisticas/mediavalorkm",
            
            "/estatisticas/maiordistancia",
            "/estatisticas/menordistancia",
            "/estatisticas/mediadistancia",
            
            "/estatisticas/maiorvalortotal",
            "/estatisticas/menorvalortotal",
            "/estatisticas/mediavalortotal",
            
            "/estatisticas/maiorvalorkmveiculomarca",
            "/estatisticas/menorvalorkmveiculomarca",
            "/estatisticas/mediavalorkmveiculomarca",
                
            "/estatisticas/top10motoristascorridas",
            "/estatisticas/top10motoristasqualificacao",
            "/estatisticas/top10motoristasganhos",
            
            "/estatisticas/top10clientescorridas",
            "/estatisticas/top10clientesqualificacao",
            "/estatisticas/top10clientesgastos",
            
            "/estatisticas/top10fidelidade"
                
        })
public class EstatisticasController extends HttpServlet{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        switch (request.getServletPath()) {
            case "/estatisticas": {
                requestDispatcher = request.getRequestDispatcher("/view/estatisticas/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/estatisticas/quantidadecorridas": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    int quantidade = daoEstatisticas.getQuantidadeCorridas();
                    request.setAttribute("quantidade", quantidade);
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/quantidadecorridas.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/maiorvalorkm": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMaiorValorKm();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/maiorvalorkm.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/menorvalorkm": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMenorValorKm();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/menorvalorkm.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/mediavalorkm": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    double valormedio = daoEstatisticas.getCorridaMediaValorKm();
                    request.setAttribute("valormedio", valormedio);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/mediavalorkm.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/maiordistancia": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMaiorDistancia();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/maiordistancia.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/menordistancia": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMenorDistancia();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/menordistancia.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/mediadistancia": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    double distanciamedia = daoEstatisticas.getCorridaMediaDistancia();
                    request.setAttribute("distanciamedia", distanciamedia);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/mediadistancia.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            
            
            case "/estatisticas/maiorvalortotal": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMaiorValorTotal();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/maiorvalortotal.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/menorvalortotal": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    Corrida corrida = daoEstatisticas.getCorridaMenorValorTotal();
                    request.setAttribute("corrida", corrida);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/menorvalortotal.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/mediavalortotal": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    double valormedio = daoEstatisticas.getCorridaMediaValorTotal();
                    request.setAttribute("valormedio", valormedio);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/mediavalortotal.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/estatisticas/maiorvalorkmveiculomarca": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getMaiorValorKmMarca();
                    request.setAttribute("resultados", resultados);
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/maiorvalorkmveiculomarca.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/menorvalorkmveiculomarca": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getMenorValorKmMarca();
                    request.setAttribute("resultados", resultados);
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/menorvalorkmveiculomarca.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                break;
            }
            case "/estatisticas/mediavalorkmveiculomarca": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getMediaValorKmMarca();
                    request.setAttribute("resultados", resultados);
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/mediavalorkmveiculomarca.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            
            
            case "/estatisticas/top10motoristascorridas": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopMotoristasCorridas();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getTaxista());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10motoristascorridas.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/top10motoristasqualificacao": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopMotoristasQualificacoes();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getTaxista());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10motoristasqualificacao.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/top10motoristasganhos": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopMotoristasGanhos();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getTaxista());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10motoristasganhos.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;   
            }
            case "/estatisticas/top10clientescorridas": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopClientesCorridas();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getCliente());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10clientescorridas.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/top10clientesqualificacao": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopClientesQualificacoes();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getCliente());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10clientesqualificacao.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatisticas/top10clientesgastos": {
                try {
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopClientesGastos();
                    request.setAttribute("resultados", resultados);
                    
                    UsuarioDAO daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Usuario usuario;
                    for(int i=0; i<resultados.size(); i++){
                        usuario = daoUsuario.read(resultados.get(i).getCliente());
                        usuarios.add(usuario);
                    }
                    request.setAttribute("usuarios", usuarios);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10clientesgastos.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } 
                break;
            }
            
           case "/estatisticas/top10fidelidade": {
                try {
                    // CONSULTA e RETORNA OS TOP 10 FIDELIDADE...
                    daoFactory = new DAOFactory();
                    EstatisticasDAO daoEstatisticas = daoFactory.getEstatisticasDAO();
                    List<Resultado> resultados = daoEstatisticas.getTopFidelidade();
                    request.setAttribute("resultados", resultados);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatisticas/top10fidelidade.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticasController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            
        }
    }
    
        
}