package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.CorridaDAO;
import dao.DAOFactory;
import dao.UsuarioDAO;
import dao.PontoDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import model.Conversor;
import model.Corrida;
import model.Usuario;
import model.Ponto;

@WebServlet(name = "CorridaController",
        urlPatterns = {
            "/corrida",
            "/corrida/destino",
            "/corrida/novaCorrida",
            "/corrida/escolhaMotorista",
            "/corrida/ticket",
            "/corrida/qualificar",
            "/corrida/accept",
            "/corrida/reject",
            "/motorista/todos",
            "/motorista/pesquisar",
            "/corrida/tragetoria"
        })

public class CorridaController extends HttpServlet{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        UsuarioDAO daoUsuario;
        CorridaDAO daoCorrida;
        
        switch (request.getServletPath()) {
            case "/corrida": {
                requestDispatcher = request.getRequestDispatcher("/view/corrida/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/ticket": {
                int id = Integer.parseInt(request.getParameter("id"));
                try {
                    daoFactory = new DAOFactory();
                    daoCorrida = daoFactory.getCorridaDAO();
                    Corrida corrida = daoCorrida.read(id);
                    request.setAttribute("corrida", corrida);
                    requestDispatcher = request.getRequestDispatcher("/view/corrida/ticket.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }   
            }
            case "/corrida/qualificar": {
                requestDispatcher = request.getRequestDispatcher("/view/corrida/qualificacaoCliente.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/accept": {
                try {
                    int id = Integer.parseInt(request.getParameter("id"));
                    daoFactory = new DAOFactory();
                    daoCorrida = daoFactory.getCorridaDAO();
                    Corrida corrida = daoCorrida.read(id);
                    // verifica se e o taxista mesmo que esta aceitando a corrida
                    int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    if(idSessao == corrida.getTaxista()){
                        Timestamp atualTimestamp = new Timestamp(System.currentTimeMillis());
                        corrida.setDataChamada(atualTimestamp);// Atualiza a data de chamada
                        corrida.setStatus(1);
                        daoCorrida.update(corrida);
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(request.getContextPath() + "/cadastro");
                break;
            }
            case "/corrida/reject": {
                try {
                    int id = Integer.parseInt(request.getParameter("id"));
                    daoFactory = new DAOFactory();
                    daoCorrida = daoFactory.getCorridaDAO();
                    Corrida corrida = daoCorrida.read(id);
                    // verifica se e o taxista mesmo que esta aceitando a corrida
                    int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    if(idSessao == corrida.getTaxista()){
                        corrida.setStatus(-1);
                        daoCorrida.update(corrida);
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                response.sendRedirect(request.getContextPath() + "/cadastro");
                break;
            }
            case "/motorista/todos": {
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> motoristas = daoUsuario.allMotoristas();
                    request.setAttribute("motoristas", motoristas);
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
                requestDispatcher = request.getRequestDispatcher("/view/motorista/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/motorista/pesquisar": {
                requestDispatcher = request.getRequestDispatcher("/view/motorista/pesquisa.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            
            case "/corrida/tragetoria": {
                try {
                    DAOFactory factory = new DAOFactory();
                    PontoDAO pontoDAO = factory.getPontoDAO();

                    // Pega a corrida
                    CorridaDAO corridaDAO = factory.getCorridaDAO();
                    Corrida corrida = corridaDAO.read(Integer.parseInt(request.getParameter("id")));
                    
                    boolean status = false;
                    if(corrida.getStatus() >= 1){
                        status = true;
                    }
                    // Pegar posicoes
                    Ponto ponto = pontoDAO.read(corrida.getPontorota());
                    List<Ponto> rota = pontoDAO.getRota(ponto.getCarro_id(), ponto.getTempo(), corrida.getDataChamada(), status);
                    
                    // Instanciando conversor de objetos java para JSON
                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    // Preparar resposta JSON
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    // Conversão JSON e escrita da resposta
                    out.print(gson.toJson(rota));
                    
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
                /*
                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                PrintWriter out = response.getWriter();
                //out.print("[{\"id\":3,\"lat\":40.02126,\"lng\":116.7539,\"observ\":\"03-02-2008 09:04:02.0\"},{\"id\":3,\"lat\":40.02126,\"lng\":116.7539,\"observ\":\"03-02-2008 09:09:01.0\"},{\"id\":3,\"lat\":40.02126,\"lng\":116.7539,\"observ\":\"03-02-2008 09:14:02.0\"},{\"id\":3,\"lat\":40.02126,\"lng\":116.7539,\"observ\":\"03-02-2008 09:19:02.0\"},{\"id\":3,\"lat\":40.05878,\"lng\":116.75033,\"observ\":\"03-02-2008 09:34:02.0\"},{\"id\":3,\"lat\":40.0606,\"lng\":116.70075,\"observ\":\"03-02-2008 09:39:02.0\"},{\"id\":3,\"lat\":40.05369,\"lng\":116.70035,\"observ\":\"03-02-2008 09:44:02.0\"},{\"id\":3,\"lat\":40.05369,\"lng\":116.70035,\"observ\":\"03-02-2008 09:44:02.0\"},{\"id\":3,\"lat\":40.03407,\"lng\":116.67169,\"observ\":\"03-02-2008 09:49:02.0\"},{\"id\":3,\"lat\":39.99409,\"lng\":116.67032,\"observ\":\"03-02-2008 09:54:01.0\"},{\"id\":3,\"lat\":39.9481,\"lng\":116.65041,\"observ\":\"03-02-2008 09:59:01.0\"},{\"id\":3,\"lat\":39.93277,\"lng\":116.65199,\"observ\":\"03-02-2008 10:04:01.0\"},{\"id\":3,\"lat\":39.92585,\"lng\":116.63368,\"observ\":\"03-02-2008 10:09:01.0\"},{\"id\":3,\"lat\":39.92505,\"lng\":116.59928,\"observ\":\"03-02-2008 10:14:01.0\"},{\"id\":3,\"lat\":39.92077,\"lng\":116.58457,\"observ\":\"03-02-2008 10:19:01.0\"},{\"id\":3,\"lat\":39.92062,\"lng\":116.58454,\"observ\":\"03-02-2008 10:24:01.0\"},{\"id\":3,\"lat\":39.9206,\"lng\":116.58465,\"observ\":\"03-02-2008 10:29:01.0\"},{\"id\":3,\"lat\":39.9205,\"lng\":116.5849,\"observ\":\"03-02-2008 10:34:02.0\"},{\"id\":3,\"lat\":39.92016,\"lng\":116.58475,\"observ\":\"03-02-2008 10:39:02.0\"},{\"id\":3,\"lat\":39.92311,\"lng\":116.55815,\"observ\":\"03-02-2008 10:54:01.0\"},{\"id\":3,\"lat\":39.92251,\"lng\":116.51462,\"observ\":\"03-02-2008 10:59:01.0\"},{\"id\":3,\"lat\":39.91451,\"lng\":116.49104,\"observ\":\"03-02-2008 11:04:01.0\"},{\"id\":3,\"lat\":39.91012,\"lng\":116.48706,\"observ\":\"03-02-2008 11:09:01.0\"},{\"id\":3,\"lat\":39.91426,\"lng\":116.4837,\"observ\":\"03-02-2008 11:14:01.0\"},{\"id\":3,\"lat\":39.91426,\"lng\":116.4837,\"observ\":\"03-02-2008 11:14:06.0\"},{\"id\":3,\"lat\":39.91447,\"lng\":116.46639,\"observ\":\"03-02-2008 11:19:01.0\"},{\"id\":3,\"lat\":39.92005,\"lng\":116.44702,\"observ\":\"03-02-2008 11:24:15.0\"},{\"id\":3,\"lat\":39.92205,\"lng\":116.44056,\"observ\":\"03-02-2008 11:29:01.0\"},{\"id\":3,\"lat\":39.92714,\"lng\":116.45103,\"observ\":\"03-02-2008 11:39:01.0\"},{\"id\":3,\"lat\":39.93257,\"lng\":116.455,\"observ\":\"03-02-2008 11:44:01.0\"},{\"id\":3,\"lat\":39.93257,\"lng\":116.455,\"observ\":\"03-02-2008 11:44:01.0\"},{\"id\":3,\"lat\":39.93245,\"lng\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lat\":39.93245,\"lng\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lat\":39.93245,\"lng\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lat\":39.93182,\"lng\":116.48602,\"observ\":\"03-02-2008 11:54:01.0\"},{\"id\":3,\"lat\":39.93613,\"lng\":116.50056,\"observ\":\"03-02-2008 11:59:01.0\"},{\"id\":3,\"lat\":39.9403,\"lng\":116.53189,\"observ\":\"03-02-2008 12:04:01.0\"}]");
                //out.print("[{\"id\":3,\"lati\":40.02126,\"long\":116.7539,\"observ\":\"03-02-2008 09:04:02.0\"},{\"id\":3,\"lati\":40.02126,\"long\":116.7539,\"observ\":\"03-02-2008 09:09:01.0\"},{\"id\":3,\"lati\":40.02126,\"long\":116.7539,\"observ\":\"03-02-2008 09:14:02.0\"},{\"id\":3,\"lati\":40.02126,\"long\":116.7539,\"observ\":\"03-02-2008 09:19:02.0\"},{\"id\":3,\"lati\":40.05878,\"long\":116.75033,\"observ\":\"03-02-2008 09:34:02.0\"},{\"id\":3,\"lati\":40.0606,\"long\":116.70075,\"observ\":\"03-02-2008 09:39:02.0\"},{\"id\":3,\"lati\":40.05369,\"long\":116.70035,\"observ\":\"03-02-2008 09:44:02.0\"},{\"id\":3,\"lati\":40.05369,\"long\":116.70035,\"observ\":\"03-02-2008 09:44:02.0\"},{\"id\":3,\"lati\":40.03407,\"long\":116.67169,\"observ\":\"03-02-2008 09:49:02.0\"},{\"id\":3,\"lati\":39.99409,\"long\":116.67032,\"observ\":\"03-02-2008 09:54:01.0\"},{\"id\":3,\"lati\":39.9481,\"long\":116.65041,\"observ\":\"03-02-2008 09:59:01.0\"},{\"id\":3,\"lati\":39.93277,\"long\":116.65199,\"observ\":\"03-02-2008 10:04:01.0\"},{\"id\":3,\"lati\":39.92585,\"long\":116.63368,\"observ\":\"03-02-2008 10:09:01.0\"},{\"id\":3,\"lati\":39.92505,\"long\":116.59928,\"observ\":\"03-02-2008 10:14:01.0\"},{\"id\":3,\"lati\":39.92077,\"long\":116.58457,\"observ\":\"03-02-2008 10:19:01.0\"},{\"id\":3,\"lati\":39.92062,\"long\":116.58454,\"observ\":\"03-02-2008 10:24:01.0\"},{\"id\":3,\"lati\":39.9206,\"long\":116.58465,\"observ\":\"03-02-2008 10:29:01.0\"},{\"id\":3,\"lati\":39.9205,\"long\":116.5849,\"observ\":\"03-02-2008 10:34:02.0\"},{\"id\":3,\"lati\":39.92016,\"long\":116.58475,\"observ\":\"03-02-2008 10:39:02.0\"},{\"id\":3,\"lati\":39.92311,\"long\":116.55815,\"observ\":\"03-02-2008 10:54:01.0\"},{\"id\":3,\"lati\":39.92251,\"long\":116.51462,\"observ\":\"03-02-2008 10:59:01.0\"},{\"id\":3,\"lati\":39.91451,\"long\":116.49104,\"observ\":\"03-02-2008 11:04:01.0\"},{\"id\":3,\"lati\":39.91012,\"long\":116.48706,\"observ\":\"03-02-2008 11:09:01.0\"},{\"id\":3,\"lati\":39.91426,\"long\":116.4837,\"observ\":\"03-02-2008 11:14:01.0\"},{\"id\":3,\"lati\":39.91426,\"long\":116.4837,\"observ\":\"03-02-2008 11:14:06.0\"},{\"id\":3,\"lati\":39.91447,\"long\":116.46639,\"observ\":\"03-02-2008 11:19:01.0\"},{\"id\":3,\"lati\":39.92005,\"long\":116.44702,\"observ\":\"03-02-2008 11:24:15.0\"},{\"id\":3,\"lati\":39.92205,\"long\":116.44056,\"observ\":\"03-02-2008 11:29:01.0\"},{\"id\":3,\"lati\":39.92714,\"long\":116.45103,\"observ\":\"03-02-2008 11:39:01.0\"},{\"id\":3,\"lati\":39.93257,\"long\":116.455,\"observ\":\"03-02-2008 11:44:01.0\"},{\"id\":3,\"lati\":39.93257,\"long\":116.455,\"observ\":\"03-02-2008 11:44:01.0\"},{\"id\":3,\"lati\":39.93245,\"long\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lati\":39.93245,\"long\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lati\":39.93245,\"long\":116.47199,\"observ\":\"03-02-2008 11:49:01.0\"},{\"id\":3,\"lati\":39.93182,\"long\":116.48602,\"observ\":\"03-02-2008 11:54:01.0\"},{\"id\":3,\"lati\":39.93613,\"long\":116.50056,\"observ\":\"03-02-2008 11:59:01.0\"},{\"id\":3,\"lati\":39.9403,\"long\":116.53189,\"observ\":\"03-02-2008 12:04:01.0\"}]");
                break;
                */
            }
           
            
            
        }
            
    }
    
    
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        UsuarioDAO daoUsuario;
        CorridaDAO daoCorrida;
        
        switch (request.getServletPath()) {
            case "/corrida": {
                requestDispatcher = request.getRequestDispatcher("/view/corrida/index.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/destino": {
                requestDispatcher = request.getRequestDispatcher("/view/corrida/destino.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/novaCorrida": {
                // Cria tipo corrida
                int idTaxista = Integer.parseInt(request.getParameter("id"));
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    Usuario taxista = daoUsuario.read(idTaxista);
                    Usuario usuarioSessao = (Usuario)request.getSession().getAttribute("sessao");
                    
                    Corrida corrida = new Corrida();
                    corrida.setTaxista(taxista.getId()); // PEGA PELO PARAMETRO
                    corrida.setCliente(usuarioSessao.getId()); // PEGA PELA SESSAO
                    corrida.setVeiculo(taxista.getVeiculo()); // FAZ CONSULTA PARA VER O CARRO DO TAXISTA
                    corrida.setValorkm(taxista.getValorkm()); // FAZ CONSULTA PARA VER O VALOR DO TAXISTA
                    
                    double latA = Double.parseDouble(request.getParameter("latitude_origem"));
                    double lngA = Double.parseDouble(request.getParameter("longitude_origem"));
                    double latB = Double.parseDouble(request.getParameter("latitude_destino"));
                    double lngB = Double.parseDouble(request.getParameter("longitude_destino"));
                    corrida.setDistancia(Conversor.calcDistance(latA,lngA,latB,lngB)); // TRIGGER CALCULA
                    // encontra a (latitude e longitude) inicial
                    corrida.setLocalizacao_A_latitude(request.getParameter("latitude_origem")); // 
                    corrida.setLocalizacao_A_longitude(request.getParameter("longitude_origem"));
                    corrida.setLocalizacao_B_latitude(request.getParameter("latitude_destino"));
                    corrida.setLocalizacao_B_longitude(request.getParameter("longitude_destino")); //
                    corrida.setPontorota(Integer.parseInt(request.getParameter("ponto_proximo")));
                    //corrida.setQualificacao_motorista("Qualificacao atribuida pelo motorista"); // NULL
                    corrida.setEstrelas_motorista(0); // NULL
                    //corrida.setQualificacao_cliente("Qualificacao dada pelo cliente pelo servico!"); // NULL
                    corrida.setEstrelas_cliente(0); // NULL
                    corrida.setStatus(0); // 0 DEFAULT
                    
                   
                    daoCorrida = daoFactory.getCorridaDAO();
                    int id = daoCorrida.create(corrida);
                    response.sendRedirect(request.getContextPath() + "/corrida/ticket?id="+id);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/corrida/escolhaMotorista": {
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    
                    List<Usuario> motoristas = daoUsuario.allMotoristas();
                    request.setAttribute("motoristas", motoristas);
                    
                    daoCorrida = daoFactory.getCorridaDAO();
                    
                    float latitude = Float.parseFloat(request.getParameter("latitude_origem"));
                    float longitude = Float.parseFloat(request.getParameter("longitude_origem"));
                    List<Ponto> pontosProximos = daoCorrida.recuperarRota(longitude, latitude);
                    int quantidade = pontosProximos.size();
                    
                    request.setAttribute("quantidade", quantidade);
                    request.setAttribute("pontos_proximos", pontosProximos);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
                requestDispatcher = request.getRequestDispatcher("/view/corrida/chooseDriver.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/corrida/qualificar": {
                try {
                    int id = Integer.parseInt(request.getParameter("id"));
                    daoFactory = new DAOFactory();
                    daoCorrida = daoFactory.getCorridaDAO();
                    Corrida corrida = daoCorrida.read(id);
                            
                    corrida.setQualificacao_cliente(request.getParameter("qualificacao"));
                    corrida.setEstrelas_cliente(Integer.parseInt(request.getParameter("estrelas")));
                    corrida.setStatus(2);
                            
                    daoCorrida.update(corrida);
                    
                    response.sendRedirect(request.getContextPath() + "/corrida/ticket?id="+id);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                break;
            }
            case "/motorista/pesquisar": {
                try {
                    daoFactory = new DAOFactory();
                    daoUsuario = daoFactory.getUsuarioDAO();
                    List<Usuario> motoristas = daoUsuario.pesquisarMotoristas(Float.parseFloat(request.getParameter("preco_de")),Float.parseFloat(request.getParameter("preco_ate")),request.getParameter("nome"),request.getParameter("sobrenome"),request.getParameter("marca"),request.getParameter("modelo"));
                    request.setAttribute("motoristas", motoristas);
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                } catch (SQLException ex) {
                    Logger.getLogger(CorridaController.class.getName()).log(Level.SEVERE, null, ex);
                    break;
                }
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/resultadoPesquisa.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
        }
    }

}
