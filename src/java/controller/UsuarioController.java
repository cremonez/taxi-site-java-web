package controller;

import dao.DAOFactory;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import dao.CorridaDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Usuario;
import model.Veiculo;
import model.Corrida;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import model.Resultado;



@WebServlet(name = "UsuarioController",
        urlPatterns = {
            "/cadastro",
            "/cadastro/login",
            "/cadastro/create",
            "/cadastro/edit",
            "/cadastro/delete",
            "/cadastro/perfil",
            "/cadastro/upgrade",
            "/cadastro/logout",
            "/cadastro/configuracoes",
            "/cadastro/editDriver",
            "/cadastro/editImageProfile",
            "/cadastro/editImageCar",
            "/cadastro/callDriver"
        })
public class UsuarioController extends HttpServlet {
    private Object estatisticaDao;
           
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        UsuarioDAO usuarioDao;
        VeiculoDAO veiculoDao;

        switch (request.getServletPath()) {
            case "/cadastro": {
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    
                    Usuario recuperaSessao = (Usuario)request.getSession().getAttribute("sessao");
                    if(recuperaSessao == null){
                        response.sendRedirect(request.getContextPath() + "/cadastro/login");
                        break;
                    }
                    int id = recuperaSessao.getId();
                    
                    Usuario usuario = usuarioDao.read(id);
                    request.setAttribute("usuario", usuario);
                    
                    if(usuario.getValorkm() != -1){
                        veiculoDao = daoFactory.getVeiculoDAO();
                        Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                        request.setAttribute("veiculo", veiculo);
                    }
                    // recupera as corridas solicitadas
                    CorridaDAO corridaDao = daoFactory.getCorridaDAO();
                    List<Corrida> corridasSolicitadas = corridaDao.recuperarCorridasSolicidatas(usuario.getId());
                    request.setAttribute("corridasSolicitadas", corridasSolicitadas);
                    // recupera as solicitacoes de corrida recebidas (motorista)
                    List<Corrida> corridasRecebidas = corridaDao.recuperarCorridasRecebidas(usuario.getId());
                    request.setAttribute("corridasRecebidas", corridasRecebidas);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/cadastro/index.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
               // requestDispatcher = request.getRequestDispatcher("/view/cadastro/index.jsp");
               // requestDispatcher.forward(request, response);
                break;
            }
            case "/cadastro/create": {    
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/cadastro/configuracoes": {
                int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    veiculoDao = daoFactory.getVeiculoDAO();
                    Usuario usuario = usuarioDao.read(idSessao);
                    Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                    request.setAttribute("veiculo", veiculo);
                    requestDispatcher = request.getRequestDispatcher("/view/cadastro/configuracoes.jsp");
                    requestDispatcher.forward(request, response);
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/cadastro/login": {    
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/login.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/cadastro/logout": {
                HttpSession session = request.getSession(false);
                session.invalidate();
                response.sendRedirect(request.getContextPath() + "/cadastro");
                break;
            }
            
            case "/cadastro/edit": {    
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/edit.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/cadastro/editDriver": {
               try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    
                    Usuario recuperaSessao = (Usuario)request.getSession().getAttribute("sessao");
                    if(recuperaSessao == null){
                        response.sendRedirect(request.getContextPath() + "/cadastro/login");
                        break;
                    }
                    int id = recuperaSessao.getId();
                    
                    Usuario usuario = usuarioDao.read(id);
                    request.setAttribute("usuario", usuario);
                    
                    if(usuario.getValorkm() != -1){
                        veiculoDao = daoFactory.getVeiculoDAO();
                        Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                        request.setAttribute("veiculo", veiculo);
                    }
                    requestDispatcher = request.getRequestDispatcher("/view/cadastro/editDriver.jsp");
                    requestDispatcher.forward(request, response);
                    break;
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/cadastro/upgrade": {    
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/upgrade.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/cadastro/perfil": {
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    int id = Integer.parseInt(request.getParameter("id"));
                    Usuario usuario = usuarioDao.read(id);
                    List<Resultado> resultados = usuarioDao.getListMotoristaQualificacoes(id);
                    request.setAttribute("resultados", resultados);
                    request.setAttribute("usuario", usuario);
                    
                    if(usuario.getValorkm() == -1){
                        requestDispatcher = request.getRequestDispatcher("/view/passageiro/perfil.jsp");
                        requestDispatcher.forward(request, response);
                    }else{
                        veiculoDao = daoFactory.getVeiculoDAO();
                        Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                        request.setAttribute("veiculo", veiculo);
                        
                        requestDispatcher = request.getRequestDispatcher("/view/motorista/perfil.jsp");
                        requestDispatcher.forward(request, response);
                    }   
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/cadastro/editImageProfile": {    
                requestDispatcher = request.getRequestDispatcher("/view/cadastro/editImageProfile.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/cadastro/editImageCar": {
                int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    veiculoDao = daoFactory.getVeiculoDAO();
                    Usuario usuario = usuarioDao.read(idSessao);
                    Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                    request.setAttribute("veiculo", veiculo);
                    requestDispatcher = request.getRequestDispatcher("/view/cadastro/editImageCar.jsp");
                    requestDispatcher.forward(request, response);
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            
            
        }
    }
    
    
    
    
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        UsuarioDAO dao;
        VeiculoDAO daoVeiculo;
        UsuarioDAO usuarioDao;
        
        switch (request.getServletPath()) {
            case "/cadastro/create": {
                if(request.getParameter("taxi_email") == "" || request.getParameter("taxi_senha") == "" || request.getParameter("nome") == "" || request.getParameter("sobrenome") == "" || request.getParameter("nascimento") == "" || request.getParameter("telefone") == "" || request.getParameter("sexo") == "" || request.getParameter("cpf") == ""){
                    response.sendRedirect(request.getContextPath() + "/cadastro/create?error=1");
                    break;
                }else{
                    Usuario usuario = new Usuario();
                    usuario.setEmail(request.getParameter("taxi_email"));
                    usuario.setSenha(request.getParameter("taxi_senha"));
                    usuario.setNome(request.getParameter("nome"));
                    usuario.setSobrenome(request.getParameter("sobrenome"));
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Date dataNascimento;
                    try {
                        dataNascimento = dateFormat.parse(request.getParameter("nascimento"));
                        usuario.setNascimento(new java.sql.Date(dataNascimento.getTime()));
                    } catch (ParseException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //usuario.setNascimento(Date.pa(request.getParameter("nascimento")));
                    usuario.setTelefone(request.getParameter("telefone"));
                    usuario.setSexo(Boolean.parseBoolean(request.getParameter("sexo")));
                    usuario.setCpf(request.getParameter("cpf"));
                    usuario.setValorkm(-1);
                    try {
                        
                        daoFactory = new DAOFactory();
                        dao = daoFactory.getUsuarioDAO();
                        int id = dao.create(usuario);
                        response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+id);
                    } catch (ClassNotFoundException | SQLException ex) {
                        Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
            
            case "/cadastro/upgrade": {    
                Veiculo veiculo = new Veiculo();
                veiculo.setMarca(request.getParameter("marca"));
                veiculo.setModelo(request.getParameter("modelo"));
                veiculo.setCor(request.getParameter("cor"));     
                veiculo.setCapacidade(Integer.parseInt(request.getParameter("capacidade"))); 
                veiculo.setRenavam(request.getParameter("renavam")); 
                veiculo.setPlaca(request.getParameter("placa")); 
                veiculo.setAno(Integer.parseInt(request.getParameter("ano"))); 
                
                try {
                    daoFactory = new DAOFactory();
                    daoVeiculo = daoFactory.getVeiculoDAO();
                    int idVeiculo = daoVeiculo.create(veiculo);
                    // Faz a ligacao usuario <-> veiculo
                    int id = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    Usuario usuario = usuarioDao.read(id);
                    usuario.setValorkm(Float.parseFloat(request.getParameter("preco")));
                    usuario.setVeiculo(idVeiculo);
                    usuarioDao.updateSemSenha(usuario);
                    
                    /// Atualiza sessao
                    usuario = usuarioDao.read(id);
                    HttpSession sessao;
                    sessao= request.getSession(false);
                    sessao.invalidate();
                    sessao = request.getSession(true);
                    sessao.setAttribute("sessao", usuario);
                    
                    response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+id);
                } catch (ClassNotFoundException | SQLException ex) {
                    response.sendRedirect(request.getContextPath() + "/cadastro");
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                break;
            }
            
            case "/cadastro/edit": {
                
                Usuario usuario = new Usuario();
                int id = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                usuario.setId(id);
                usuario.setEmail(request.getParameter("taxi_email"));
                usuario.setSenha(request.getParameter("taxi_senha"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setSobrenome(request.getParameter("sobrenome"));
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date dataNascimento;
                try {
                    dataNascimento = dateFormat.parse(request.getParameter("nascimento"));
                    usuario.setNascimento(new java.sql.Date(dataNascimento.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                //usuario.setNascimento(Integer.parseInt(request.getParameter("nascimento")));
                usuario.setTelefone(request.getParameter("telefone"));
                usuario.setSexo(Boolean.parseBoolean(request.getParameter("sexo")));
                usuario.setCpf(request.getParameter("cpf"));
                usuario.setValorkm(-1);
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    
                    if(usuario.getSenha() == ""){
                        dao.updateSemVeiculoSemSenha(usuario);
                    }else{
                        dao.updateSemVeiculo(usuario);
                    }
                    
                    HttpSession sessao;
                    sessao= request.getSession(false);
                    sessao.invalidate();
                    
                    sessao = request.getSession(true);
                    sessao.setAttribute("sessao", usuario);
                    response.sendRedirect(request.getContextPath() + "/cadastro");
                    
                } catch (ClassNotFoundException | SQLException ex) {
                   Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                break;
               
            }
            case "/cadastro/editDriver": {
                //////////////// DADOS DE PASSAGEIRO ////////////////
                Usuario usuario = new Usuario();
                int id = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                usuario.setId(id);
                usuario.setEmail(request.getParameter("taxi_email"));
                usuario.setSenha(request.getParameter("taxi_senha"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setSobrenome(request.getParameter("sobrenome"));
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date dataNascimento;
                try {
                    dataNascimento = dateFormat.parse(request.getParameter("nascimento"));
                    usuario.setNascimento(new java.sql.Date(dataNascimento.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                //usuario.setNascimento(Integer.parseInt(request.getParameter("nascimento")));
                usuario.setTelefone(request.getParameter("telefone"));
                usuario.setSexo(Boolean.parseBoolean(request.getParameter("sexo")));
                usuario.setCpf(request.getParameter("cpf"));
                usuario.setValorkm(Float.parseFloat(request.getParameter("preco")));
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    if(usuario.getSenha() == ""){
                        dao.updateSemVeiculoSemSenha(usuario);
                    }else{
                        dao.updateSemVeiculo(usuario);
                    }
                    
                    HttpSession sessao;
                    sessao= request.getSession(false);
                    sessao.invalidate();
                    
                    sessao = request.getSession(true);
                    sessao.setAttribute("sessao", usuario);
                    
                    //response.sendRedirect(request.getContextPath() + "/cadastro");
                    
                } catch (ClassNotFoundException | SQLException ex) {
                   Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                //////////////// DADOS DE MOTORISTA /////////////////////////////
                
                Veiculo veiculo = new Veiculo();
                
                veiculo.setMarca(request.getParameter("marca"));
                veiculo.setModelo(request.getParameter("modelo"));
                veiculo.setCor(request.getParameter("cor"));     
                veiculo.setCapacidade(Integer.parseInt(request.getParameter("capacidade"))); 
                veiculo.setRenavam(request.getParameter("renavam")); 
                veiculo.setPlaca(request.getParameter("placa")); 
                veiculo.setAno(Integer.parseInt(request.getParameter("ano"))); 
                
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    usuario = dao.read(id);
                    veiculo.setId(usuario.getVeiculo());
                    
                    daoFactory = new DAOFactory();
                    daoVeiculo = daoFactory.getVeiculoDAO();
                    daoVeiculo.update(veiculo);
                    
                    
                    response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+id);
                } catch (ClassNotFoundException | SQLException ex) {
                    response.sendRedirect(request.getContextPath() + "/cadastro");
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                break;
            }
            
            
            case "/cadastro/delete": {
                //Faz o updade do status para -1
                try{
                    int id = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    usuarioDao.disableAccount(id);
                    response.sendRedirect(request.getContextPath() + "/cadastro/logout");
                    
                }catch (ClassNotFoundException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            
            case "/cadastro/login": {
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();

                    Usuario usuario = dao.authenticate(
                        request.getParameter("taxi_email"),
                        request.getParameter("taxi_senha")
                    );
                    
                    
                    if (usuario != null) {
                        if(usuario.getStatus() == -1){
                            response.sendRedirect(request.getContextPath() + "/cadastro/login?error=404");
                            break;
                        }
                        
                        HttpSession sessao = request.getSession(true);
                        sessao.setAttribute("sessao", usuario);
                        response.sendRedirect(request.getContextPath() + "/cadastro");
                    } else {
                        response.sendRedirect(request.getContextPath() + "/cadastro/login");
                    }

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
            
            
            
        }

    }

    


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}