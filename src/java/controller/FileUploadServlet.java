/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.DAOFactory;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Usuario;
import model.Veiculo;

@WebServlet(name = "FileUploadServlet", 
        urlPatterns = {
            "/cadastro/updateImageProfile",
            "/cadastro/updateImageCar"
            
        })
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(FileUploadServlet.class.getCanonicalName());
        
    
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

        switch (request.getServletPath()) {
            case "/cadastro/updateImageProfile": {
                response.setContentType("text/html;charset=UTF-8");
                // Create path components to save the file
                final String path = "okayokay";//request.getParameter("fotografia");
                final Part filePart = request.getPart("fotografia");
                final String fileName = getFileName(filePart);

                OutputStream out = null;
                InputStream filecontent = null;
                final PrintWriter writer = response.getWriter();

                try {
                    Usuario recuperaSessao = (Usuario)request.getSession().getAttribute("sessao");
                    int id = recuperaSessao.getId();
                    String urlLocal = "/includes/media/perfil/";
                    urlLocal = getServletContext().getRealPath(urlLocal);//pageContext.servletContext.contextPath
                    out = new FileOutputStream(new File(urlLocal + File.separator + id + ".png"));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    writer.println("New file " + fileName + " created at " + path);
                    LOGGER.log(Level.INFO, "File{0}being uploaded to {1}", 
                            new Object[]{fileName, path});
                    
                    
                    // Atualiza a fotografia
                    int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    DAOFactory daoFactory = new DAOFactory();
                    UsuarioDAO usuarioDao = daoFactory.getUsuarioDAO();
                    Usuario usuario = usuarioDao.read(id);
                    usuario.setFotografia(idSessao+".png");
                    usuarioDao.updateFotografia(usuario);
                    
                    
                    response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+idSessao);
                    
                } catch (FileNotFoundException fne) {
                    writer.println("You either did not specify a file to upload or are "
                            + "trying to upload a file to a protected or nonexistent "
                            + "location.");
                    writer.println("<br/> ERROR: " + fne.getMessage());

                    LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", 
                            new Object[]{fne.getMessage()});
                } catch (ClassNotFoundException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    if (out != null) {
                        out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                }
             break;   
            }
            /*
            case "/cadastro/updateImageCar": {
                response.setContentType("text/html;charset=UTF-8");
                // Create path components to save the file
                final String path = "okayokay";//request.getParameter("fotografia");
                final Part filePart = request.getPart("fotografia");
                final String fileName = getFileName(filePart);

                OutputStream out = null;
                InputStream filecontent = null;
                final PrintWriter writer = response.getWriter();

                try {
                    
                    
                    Usuario recuperaSessao = (Usuario)request.getSession().getAttribute("sessao");
                    int id = recuperaSessao.getId();
                    String urlLocal = "/includes/media/carro/";
                    urlLocal = getServletContext().getRealPath(urlLocal);//pageContext.servletContext.contextPath
                    out = new FileOutputStream(new File(urlLocal + File.separator + id + ".png"));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    writer.println("New file " + fileName + " created at " + path);
                    LOGGER.log(Level.INFO, "File{0}being uploaded to {1}", 
                            new Object[]{fileName, path});
                    
                    
                    // Atualiza a fotografia
                    int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    DAOFactory daoFactory = new DAOFactory();
                    UsuarioDAO usuarioDao = daoFactory.getUsuarioDAO();
                    VeiculoDAO veiculoDao = daoFactory.getVeiculoDAO();
                    Usuario usuario = usuarioDao.read(idSessao);
                    Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                    
                    usuario.setFotografia(idSessao+".png");
                    usuarioDao.updateFotografia(usuario);
                    veiculo.setFotografia(veiculo.getId()+".png");
                    veiculoDao.updateFotografia(veiculo);
                    
                    
                    response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+idSessao);
                    
                } catch (FileNotFoundException fne) {
                    writer.println("You either did not specify a file to upload or are "
                            + "trying to upload a file to a protected or nonexistent "
                            + "location.");
                    writer.println("<br/> ERROR: " + fne.getMessage());

                    LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", 
                            new Object[]{fne.getMessage()});
                } catch (ClassNotFoundException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    if (out != null) {
                        out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                }
             break;   
            }
            */
            case "/cadastro/updateImageCar": {
                response.setContentType("text/html;charset=UTF-8");
                // Create path components to save the file
                final String path = "okayokay";//request.getParameter("fotografia");
                final Part filePart = request.getPart("fotografia");
                final String fileName = getFileName(filePart);

                OutputStream out = null;
                InputStream filecontent = null;
                final PrintWriter writer = response.getWriter();

                try {
                    //Usuario recuperaSessao = (Usuario)request.getSession().getAttribute("sessao");
                    //int id = recuperaSessao.getId();
                    
                    int idSessao = ((Usuario)request.getSession().getAttribute("sessao")).getId();
                    DAOFactory daoFactory = new DAOFactory();
                    UsuarioDAO usuarioDao = daoFactory.getUsuarioDAO();
                    VeiculoDAO veiculoDao = daoFactory.getVeiculoDAO();
                    Usuario usuario = usuarioDao.read(idSessao);
                    Veiculo veiculo = veiculoDao.read(usuario.getVeiculo());
                    
                    
                    String urlLocal = "/includes/media/carro/";
                    urlLocal = getServletContext().getRealPath(urlLocal);//pageContext.servletContext.contextPath
                    out = new FileOutputStream(new File(urlLocal + File.separator + veiculo.getId() + ".png"));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    writer.println("New file " + fileName + " created at " + path);
                    LOGGER.log(Level.INFO, "File{0}being uploaded to {1}", 
                            new Object[]{fileName, path});
                    
                    
                    
                    veiculo.setFotografia(veiculo.getId()+".png");
                    veiculoDao.updateFotografia(veiculo);
                    
                    response.sendRedirect(request.getContextPath() + "/cadastro/perfil?id="+idSessao);
                    
                } catch (FileNotFoundException fne) {
                    writer.println("You either did not specify a file to upload or are "
                            + "trying to upload a file to a protected or nonexistent "
                            + "location.");
                    writer.println("<br/> ERROR: " + fne.getMessage());

                    LOGGER.log(Level.SEVERE, "Problems during file upload. Error: {0}", 
                            new Object[]{fne.getMessage()});
                } catch (ClassNotFoundException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    if (out != null) {
                        out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
                    if (writer != null) {
                        writer.close();
                    }
                }
            }
            
            
            
            
        }
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

}