package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Veiculo;


public class VeiculoDAO extends DAO<Veiculo> {
    
    private final String createQuery = "INSERT INTO taxi.veiculo(marca, modelo, cor, capacidade, renavam, placa, ano) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String allQuery = "SELECT id, nome FROM taxi.veiculo;";
    
    private final String readQuery = "SELECT * FROM taxi.veiculo WHERE id = ?;";
    
    private final String updateQuery = "UPDATE taxi.veiculo "
                                       + "SET marca = ?, modelo = ?, cor = ?, capacidade = ?, renavam = ?, placa = ?, ano = ? "
                                       + "WHERE id = ?;";
            
    private final String deleteQuery  = "DELETE FROM taxi.veiculo WHERE id = ?;";
    
    private final String authenticateQuery = "SELECT * "
                                           + "FROM taxi.veiculo "
                                            + "WHERE email = ? AND senha = md5(?);";
    

    public VeiculoDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public int create(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, veiculo.getMarca());
            statement.setString(2, veiculo.getModelo());
            statement.setString(3, veiculo.getCor());
            statement.setInt(4, veiculo.getCapacidade());
            statement.setString(5, veiculo.getRenavam());
            statement.setString(6, veiculo.getPlaca());
            statement.setInt(7, veiculo.getAno());
      
      
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
            }
            return veiculo.getId();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public Veiculo read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setCor(result.getString("cor"));     
                veiculo.setCapacidade(result.getInt("capacidade")); 
                veiculo.setRenavam(result.getString("renavam")); 
                veiculo.setPlaca(result.getString("placa")); 
                veiculo.setAno(result.getInt("ano"));
                veiculo.setFotografia(result.getString("fotografia"));
            }
            
            return veiculo;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Veiculo veiculo) {
        try {
            
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, veiculo.getMarca());
            statement.setString(2, veiculo.getModelo());
            statement.setString(3, veiculo.getCor());
            statement.setInt(4, veiculo.getCapacidade());
            statement.setString(5, veiculo.getRenavam());
            statement.setString(6, veiculo.getPlaca());
            statement.setInt(7, veiculo.getAno());
            statement.setInt(8, veiculo.getId());
         
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void updateFotografia(Veiculo veiculo){
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement("UPDATE taxi.veiculo SET fotografia=? WHERE id = ?;");
            statement.setString(1, veiculo.getFotografia());
            statement.setInt(2, veiculo.getId());
            
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Veiculo> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Veiculo> usuarios = new ArrayList<Veiculo>();
            
            Veiculo usuario;
            
            while (result.next()) {
                usuario = new Veiculo();
                
                //usuario.setId(result.getInt("id"));
                //usuario.setNome(result.getString("nome"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public Veiculo authenticate(String email, String senha) {
         try {
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            
            statement.setString(1, email);
            statement.setString(2, senha);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                Veiculo veiculo = new Veiculo();
                /*
                veiculo.setId(result.getInt("id"));
                veiculo.setEmail(result.getString("email"));
                veiculo.setSenha(result.getString("senha"));
                veiculo.setNome(result.getString("nome"));
                veiculo.setSobrenome(result.getString("sobrenome"));
                veiculo.setNascimento(result.getInt("nascimento"));
                veiculo.setTelefone(result.getString("telefone"));
                veiculo.setSexo(result.getBoolean("sexo"));
                veiculo.setCpf(result.getString("cpf"));
                veiculo.setValorkm(result.getFloat("valorkm"));
                */
                return veiculo;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }

    
    
}
