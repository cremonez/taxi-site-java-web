/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Resultado;
import model.Usuario;


public class UsuarioDAO extends DAO<Usuario> {
    
    private final String createQuery = "INSERT INTO taxi.usuario(email, senha, nome, sobrenome, nascimento, telefone, cpf, sexo, valorkm, status) "
            + "VALUES (?, md5(?), ?, ?, ?, ?, ?, ?, ?, 1) RETURNING id;";
    
    private final String allQuery = "SELECT id, nome FROM taxi.usuario;";
    
    private final String allMotoristasQuery = "SELECT * FROM taxi.usuario WHERE valorkm >= 0;";
    
    private final String readQuery = "SELECT * FROM taxi.usuario WHERE id = ?;";
    
    private final String updateQuery = "UPDATE taxi.usuario "
    + "SET email = ?, senha = md5(?), nome = ?, sobrenome = ?, nascimento = ?, telefone = ?, cpf = ?, sexo = ?, veiculo = ?, valorkm = ?"
    + "WHERE id = ?;";
    
    private final String updateQuery2 = "UPDATE taxi.usuario "
    + "SET email = ?, nome = ?, sobrenome = ?, nascimento = ?, telefone = ?, cpf = ?, sexo = ?, veiculo = ?, valorkm = ?"
    + "WHERE id = ?;";
    
    private final String updateQuery3 = "UPDATE taxi.usuario "
    + "SET email = ?, senha = md5(?), nome = ?, sobrenome = ?, nascimento = ?, telefone = ?, cpf = ?, sexo = ?, valorkm = ?"
    + "WHERE id = ?;";
    
    private final String updateQuery4 = "UPDATE taxi.usuario "
    + "SET email = ?, nome = ?, sobrenome = ?, nascimento = ?, telefone = ?, cpf = ?, sexo = ?, valorkm = ?"
    + "WHERE id = ?;";
    
    private final String disableQuery = "UPDATE taxi.usuario SET status = -1 WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM taxi.usuario WHERE id = ?;";
    
    private final String authenticateQuery = "SELECT * "
                                           + "FROM taxi.usuario "
                                           + "WHERE email = ? AND senha = md5(?);";
    
    private final String pesquisarUsuario = "SELECT * FROM taxi.usuario, taxi.veiculo WHERE (usuario.veiculo = veiculo.id) AND ((valorkm >= ? AND valorkm <= ?) OR nome = ? OR sobrenome = ? OR marca = ? OR modelo = ?);";
    
    //Consulta as qualificacoes do taxista
    private final String qualificacoesTaxista = "SELECT qualificacao_cliente, estrelascliente, cliente FROM taxi.corrida WHERE status = 2 AND taxista = ?";

    public UsuarioDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public int create(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, usuario.getEmail());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getNome());
            statement.setString(4, usuario.getSobrenome());
            statement.setDate(5, usuario.getNascimento());
            statement.setString(6, usuario.getTelefone());
            statement.setString(7, usuario.getCpf());
            statement.setBoolean(8, usuario.getSexo());
            statement.setFloat(9, usuario.getValorkm());
            
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
            }
            return usuario.getId();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public Usuario read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setNascimento(result.getDate("nascimento"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setSexo(result.getBoolean("sexo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setValorkm(result.getFloat("valorkm"));
                usuario.setVeiculo(result.getInt("veiculo"));
                usuario.setFotografia(result.getString("fotografia"));
            }
            
            return usuario;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Usuario usuario) {
        try {
            
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            statement.setString(1, usuario.getEmail());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getNome());
            statement.setString(4, usuario.getSobrenome());
            statement.setDate(5, usuario.getNascimento());
            statement.setString(6, usuario.getTelefone());
            statement.setString(7, usuario.getCpf());
            statement.setBoolean(8, usuario.getSexo());
            statement.setInt(9, usuario.getVeiculo());
            statement.setFloat(10, usuario.getValorkm());
            statement.setInt(11, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void updateSemSenha(Usuario usuario){
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement(updateQuery2);
            statement.setString(1, usuario.getEmail());
            //statement.setString(2, usuario.getSenha());
            statement.setString(2, usuario.getNome());
            statement.setString(3, usuario.getSobrenome());
            statement.setDate(4, usuario.getNascimento());
            statement.setString(5, usuario.getTelefone());
            statement.setString(6, usuario.getCpf());
            statement.setBoolean(7, usuario.getSexo());
            statement.setInt(8, usuario.getVeiculo());
            statement.setFloat(9, usuario.getValorkm());
            statement.setInt(10, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateSemVeiculo(Usuario usuario){
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement(updateQuery3);
            statement.setString(1, usuario.getEmail());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getNome());
            statement.setString(4, usuario.getSobrenome());
            statement.setDate(5, usuario.getNascimento());
            statement.setString(6, usuario.getTelefone());
            statement.setString(7, usuario.getCpf());
            statement.setBoolean(8, usuario.getSexo());
            //statement.setInt(8, usuario.getVeiculo());
            statement.setFloat(9, usuario.getValorkm());
            statement.setInt(10, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateSemVeiculoSemSenha(Usuario usuario){
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement(updateQuery4);
            statement.setString(1, usuario.getEmail());
            //statement.setString(2, usuario.getSenha());
            statement.setString(2, usuario.getNome());
            statement.setString(3, usuario.getSobrenome());
            statement.setDate(4, usuario.getNascimento());
            statement.setString(5, usuario.getTelefone());
            statement.setString(6, usuario.getCpf());
            statement.setBoolean(7, usuario.getSexo());
            //statement.setInt(8, usuario.getVeiculo());
            statement.setFloat(8, usuario.getValorkm());
            statement.setInt(9, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateFotografia(Usuario usuario){
        try {
            PreparedStatement statement;
            statement = connection.prepareStatement("UPDATE taxi.usuario SET fotografia=? WHERE id = ?;");
            statement.setString(1, usuario.getFotografia());
            statement.setInt(2, usuario.getId());
            
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    public void disableAccount(int id){
       try {
            PreparedStatement statement = connection.prepareStatement(disableQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<Usuario>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public List<Usuario> allMotoristas(){
        try {
            PreparedStatement statement = connection.prepareStatement(allMotoristasQuery);
            ResultSet result = statement.executeQuery();
            List<Usuario> usuarios = new ArrayList<Usuario>();
            Usuario usuario;
            while (result.next()) {
                usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setNascimento(result.getDate("nascimento"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setSexo(result.getBoolean("sexo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setValorkm(result.getFloat("valorkm"));
                usuario.setStatus(result.getInt("status"));
                usuario.setFotografia(result.getString("fotografia"));
                usuarios.add(usuario);
            }
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Usuario authenticate(String email, String senha) {
         try {
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            
            statement.setString(1, email);
            statement.setString(2, senha);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setNascimento(result.getDate("nascimento"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setSexo(result.getBoolean("sexo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setValorkm(result.getFloat("valorkm"));
                usuario.setStatus(result.getInt("status"));
                usuario.setFotografia(result.getString("fotografia"));
                return usuario;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }

    
    public List<Usuario> pesquisarMotoristas(float preco_de, float preco_ate, String nome, String sobrenome, String marca, String modelo){
        try {
            PreparedStatement statement = connection.prepareStatement(pesquisarUsuario);
            statement.setFloat(1, preco_de);
            statement.setFloat(2, preco_ate);
            statement.setString(3, nome);
            statement.setString(4, sobrenome);
            statement.setString(5, marca);
            statement.setString(6, modelo);
            
            ResultSet result = statement.executeQuery();
            List<Usuario> usuarios = new ArrayList<Usuario>();
            Usuario usuario;
            while (result.next()) {
                usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setEmail(result.getString("email"));
                usuario.setSenha(result.getString("senha"));
                usuario.setNome(result.getString("nome"));
                usuario.setSobrenome(result.getString("sobrenome"));
                usuario.setNascimento(result.getDate("nascimento"));
                usuario.setTelefone(result.getString("telefone"));
                usuario.setSexo(result.getBoolean("sexo"));
                usuario.setCpf(result.getString("cpf"));
                usuario.setValorkm(result.getFloat("valorkm"));
                usuario.setStatus(result.getInt("status"));
                usuarios.add(usuario);
            }
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    
    // Retorna a lista de qualificacoes do taxista id
    public List<Resultado> getListMotoristaQualificacoes(int id){
        try {
            PreparedStatement statement = connection.prepareStatement(qualificacoesTaxista);
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setCliente(result.getInt("cliente"));
                resultado.setQuantidade(result.getInt("estrelascliente"));
                resultado.setTexto(result.getString("qualificacao_cliente"));
                resultados.add(resultado);
            }
            return resultados;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
}
