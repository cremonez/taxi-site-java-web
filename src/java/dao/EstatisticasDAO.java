package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Corrida;
import model.Ponto;
import model.Resultado;

/**
 *
 * @author MacBook
 */
public class EstatisticasDAO extends DAO<Corrida>{
    // Consulta a quantidade de corridas ja realizadas
    private final String quantidadeCorridas = "SELECT COUNT(*) AS quantidade FROM taxi.corrida";
    // Consulta da corrida com o maior valor por km rodado
    private final String maiorvalorkm = "SELECT * FROM taxi.corrida ORDER BY valorkm DESC LIMIT 1";
    // Consulta da corrida com o maior valor por km rodado
    private final String menorvalorkm = "SELECT * FROM taxi.corrida ORDER BY valorkm ASC LIMIT 1";
    // Preço médio das corridas realizadas por Km rodado
    private final String mediavalorkm = "SELECT AVG(valorkm) AS media FROM taxi.corrida";
    
    // Consulta da corrida com a maior distancia percorrida
    private final String maiordistancia = "SELECT * FROM taxi.corrida ORDER BY distancia DESC LIMIT 1";
    // Consulta da corrida com a menor distancia percorrida
    private final String menordistancia = "SELECT * FROM taxi.corrida ORDER BY distancia ASC LIMIT 1"; 
    // Consulta da media de distancia percorrida nas corridas
    private final String mediadistancia = "SELECT AVG(distancia) AS media FROM taxi.corrida"; 
    
    // Consulta da corrida com o maior valor total cobrado
    private final String maiorvalortotal = "SELECT * FROM taxi.corrida ORDER BY valorkm * distancia DESC LIMIT 1";
    // Consulta da corrida com o maior valor total cobrado
    private final String menorvalortotal = "SELECT * FROM taxi.corrida ORDER BY valorkm * distancia ASC LIMIT 1";
    // Consulta da media do valor total cobrado das corridas
    private final String mediavalortotal = "SELECT AVG(valorkm*distancia) AS media FROM taxi.corrida";
    //........
    // Consulta dos top 10 motoristas que mais fizeram corridas
    private final String top10MotoristasCorridas = "SELECT COUNT(*) AS quantidade, taxista  FROM taxi.corrida GROUP BY taxista ORDER BY COUNT(*) DESC LIMIT 10";
    // Consulta dos top 10 motoristas que mais ganharam dinheiro
    private final String top10MotoristasGanhos = "SELECT SUM(distancia*valorkm) AS quantidade, taxista FROM TAXI.corrida GROUP BY taxista ORDER BY quantidade DESC LIMIT 10";
    // Consulta dos top 10 motoristas com as melhores qualificacoes
    private final String top10MotoristasQualificacoes = "SELECT AVG(estrelascliente) AS quantidade,taxista FROM taxi.corrida WHERE status=2 GROUP BY taxista ORDER BY quantidade DESC LIMIT 10;";
    
    // Consulta dos top 10 clients que mais fizeram corridas
    private final String top10ClientesCorridas = "SELECT COUNT(*) AS quantidade, cliente FROM taxi.corrida GROUP BY cliente ORDER BY COUNT(*) DESC LIMIT 10;";
    // Consulta dos top 10 clientes que mais gastaram dinheiro
    private final String top10ClientesGastos = "SELECT SUM(distancia*valorkm) AS quantidade, cliente FROM TAXI.corrida GROUP BY cliente ORDER BY quantidade DESC LIMIT 10";
    // Consulta dos top 10 cliente com as melhores qualificacoes
    private final String top10ClientesQualificacoes = "SELECT AVG(estrelas_motorista) AS quantidade,cliente FROM taxi.corrida WHERE status=2 GROUP BY cliente ORDER BY quantidade DESC LIMIT 10";
    //........
    // Retorna todas as marcas
    private final String todasMarcas = "SELECT DISTINCT marca FROM taxi.veiculo ORDER BY marca ASC";
    // Consulta do menor valor por km por marca
    private final String menorValorKmMarca = "SELECT valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 AND marca = ? ORDER BY valorkm ASC LIMIT 1";
    // Consulta do maior valor por km por marca
    private final String maiorValorKmMarca = "SELECT valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 AND marca = ? ORDER BY valorkm DESC LIMIT 1";
    // Consulta da media do valor por km por marca
    private final String mediaValorKmMarca = "SELECT AVG(valorkm) AS valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 GROUP BY marca ORDER BY marca ASC";
    
    // Retorna todos os modelos
    //private final String todosModelos = "SELECT DISTINCT marca FROM taxi.veiculo ORDER BY marca ASC";
    // Consulta do menor valor por km por modelo
    //private final String menorValorKmM = "SELECT valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 AND marca = ? ORDER BY valorkm ASC LIMIT 1";
    // Consulta do maior valor por km por modelo
    //private final String maiorValorKmMarca = "SELECT valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 AND marca = ? ORDER BY valorkm DESC LIMIT 1";
    // Consulta da media do valor por km por modelo
    //private final String mediaValorKmMarca = "SELECT AVG(valorkm) AS valorkm, marca FROM taxi.usuario AS usuarios JOIN taxi.veiculo AS veiculos ON usuarios.veiculo = veiculos.id  WHERE valorkm != -1 GROUP BY marca ORDER BY marca ASC";
    
    
    
    
    // Consulta da dos TOP 10 Clientes que mais fizeram corrida com um mesmo taxista
    private final String top10fidelidade = "SELECT COUNT(*) AS quantidade, taxista, cliente FROM TAXI.corrida GROUP BY cliente, taxista ORDER BY quantidade DESC LIMIT 10";
    
    
    
    
    
    
    public EstatisticasDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public int create(Corrida corrida){
        return -1;
    }
    @Override
    public Corrida read(int id){
        return null;
    }
    @Override
    public void update(Corrida corrida){
        
    }
    
    @Override
    public void delete(int id){
    
    }
            
    @Override
    public List<Corrida> all(){
        return null;
    }
    
    // RETORNA A QUANTIDADE DE CORRIDAS JA REALIZADAS
    public int getQuantidadeCorridas(){
        try {
            PreparedStatement statement = connection.prepareStatement(quantidadeCorridas);
            ResultSet result = statement.executeQuery();
            int quantidade = -1;
            if (result.next()){
                quantidade = result.getInt("quantidade");
            }
            return quantidade;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    // RETORNA A CORRIDA COM O MAIOR VALOR POR KM PERCORRIDO
    public Corrida getCorridaMaiorValorKm(){
        try {
            PreparedStatement statement = connection.prepareStatement(maiorvalorkm);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A CORRIDA COM O MENOR VALOR KM COBRADO
    public Corrida getCorridaMenorValorKm(){
        try {
            PreparedStatement statement = connection.prepareStatement(menorvalorkm);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A MEDIA DO VALOR KM DAS CORRIDAS
    public double getCorridaMediaValorKm(){
        try {
            PreparedStatement statement = connection.prepareStatement(mediavalorkm);
            ResultSet result = statement.executeQuery();
            double valormedio = -1;
            
            if (result.next()){
                valormedio = result.getFloat("media");
            }
            return valormedio;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    // RETORNA A CORRIDA COM O MAIOR DISTANCIA PERCORRIDA
    public Corrida getCorridaMaiorDistancia(){
        try {
            PreparedStatement statement = connection.prepareStatement(maiordistancia);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A CORRIDA COM O MAIOR DISTANCIA PERCORRIDA
    public Corrida getCorridaMenorDistancia(){
        try {
            PreparedStatement statement = connection.prepareStatement(menordistancia);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A MEDIA DO VALOR KM DAS CORRIDAS
    public double getCorridaMediaDistancia(){
        try {
            PreparedStatement statement = connection.prepareStatement(mediadistancia);
            ResultSet result = statement.executeQuery();
            double valormedio = -1;
            
            if (result.next()){
                valormedio = result.getFloat("media");
            }
            return valormedio;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    
    
    
    // RETORNA A CORRIDA COM O MAIOR VALOR total COBRADO
    public Corrida getCorridaMaiorValorTotal(){
        try {
            PreparedStatement statement = connection.prepareStatement(maiorvalortotal);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A CORRIDA COM O MENOR VALOR TOTAL COBRADO
    public Corrida getCorridaMenorValorTotal(){
        try {
            PreparedStatement statement = connection.prepareStatement(menorvalortotal);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA A MEDIA DO VALOR TOTAL COBRADO DAS CORRIDAS
    public double getCorridaMediaValorTotal(){
        try {
            PreparedStatement statement = connection.prepareStatement(mediavalortotal);
            ResultSet result = statement.executeQuery();
            double valormedio = -1;
            
            if (result.next()){
                valormedio = result.getFloat("media");
            }
            return valormedio;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    // RETORNA OS TOP 10 TAXISTAS QUE MAIS FIZERAM CORRIDAS
    public List<Resultado> getTopMotoristasCorridas(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10MotoristasCorridas);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                resultado.setTaxista(result.getInt("taxista"));
                //resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA OS TOP 10 TAXISTAS QUE MAIS GANHARAM DINHEIRO
    public List<Resultado> getTopMotoristasGanhos(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10MotoristasGanhos);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                resultado.setTaxista(result.getInt("taxista"));
                //resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA OS TOP 10 TAXISTA COM AS MELHORES QUALIFICACOES
    public List<Resultado> getTopMotoristasQualificacoes(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10MotoristasQualificacoes);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                resultado.setTaxista(result.getInt("taxista"));
                //resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA OS TOP 10 CLIENTES QUE MAIS FIZERAM CORRIDAS
    public List<Resultado> getTopClientesCorridas(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10ClientesCorridas);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                //resultado.setTaxista(result.getInt("taxista"));
                resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA OS TOP 10 CLIENTES QUE MAIS GANHARAM DINHEIRO
    public List<Resultado> getTopClientesGastos(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10ClientesGastos);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                //resultado.setTaxista(result.getInt("taxista"));
                resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // RETORNA OS TOP 10 TAXISTA COM AS MELHORES QUALIFICACOES
    public List<Resultado> getTopClientesQualificacoes(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10ClientesQualificacoes);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                //resultado.setTaxista(result.getInt("taxista"));
                resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    //..
    
    
    
    
    
    // RETORNA OS TOP 10 Clientes que mais fizeram corrida com um mesmo taxista
    public List<Resultado> getTopFidelidade(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10fidelidade);
            ResultSet result = statement.executeQuery();
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                resultado = new Resultado();
                resultado.setQuantidade(result.getInt("quantidade"));
                resultado.setTaxista(result.getInt("taxista"));
                resultado.setCliente(result.getInt("cliente"));
                resultados.add(resultado);
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    //menorValorKmMarca
    public List<Resultado> getMenorValorKmMarca(){
        try {
            PreparedStatement statement = connection.prepareStatement(todasMarcas);
            ResultSet result = statement.executeQuery();
            PreparedStatement statement2;
            ResultSet result2;
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                
                statement2 = connection.prepareStatement(menorValorKmMarca);
                statement2.setString(1, result.getString("marca"));
                result2 = statement2.executeQuery();
                if(result2.next()){
                    resultado = new Resultado();
                    resultado.setValorkm(result2.getFloat("valorkm"));
                    resultado.setTexto(result2.getString("marca"));
                    resultados.add(resultado);
                }
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // MAIOR VALOR POR MARCA DE CARRO
    public List<Resultado> getMaiorValorKmMarca(){
        try {
            PreparedStatement statement = connection.prepareStatement(todasMarcas);
            ResultSet result = statement.executeQuery();
            PreparedStatement statement2;
            ResultSet result2;
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                
                statement2 = connection.prepareStatement(maiorValorKmMarca);
                statement2.setString(1, result.getString("marca"));
                result2 = statement2.executeQuery();
                if(result2.next()){
                    resultado = new Resultado();
                    resultado.setValorkm(result2.getFloat("valorkm"));
                    resultado.setTexto(result2.getString("marca"));
                    resultados.add(resultado);
                }
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    // MEDIA VALOR POR MARCA DE CARRO
    public List<Resultado> getMediaValorKmMarca(){
        try {
            PreparedStatement statement = connection.prepareStatement(mediaValorKmMarca);
            ResultSet result = statement.executeQuery();
           
            List<Resultado> resultados = new ArrayList<Resultado>();
            Resultado resultado = null;
            while (result.next()) {
                
                
                resultado = new Resultado();
                resultado.setValorkm(result.getFloat("valorkm"));
                resultado.setTexto(result.getString("marca"));
                resultados.add(resultado);
                
            }
            return resultados;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    
}
