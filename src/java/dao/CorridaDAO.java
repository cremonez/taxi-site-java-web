package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Corrida;
import model.Ponto;


public class CorridaDAO extends DAO<Corrida>{
    private final String createCorrida = "INSERT INTO taxi.corrida (taxista, cliente, veiculo, valorkm, distancia, localizacao_A_latitude, localizacao_A_longitude, localizacao_B_latitude, localizacao_B_longitude, qualificacao_motorista, estrelas_motorista, qualificacao_cliente, estrelasCliente, status, pontorota)"
                                         + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String readCorrida = "SELECT * FROM taxi.corrida WHERE id = ?;";
    
    private final String recuperarCorridasSolicidatasQuery = "SELECT * FROM taxi.corrida WHERE cliente = ? ORDER BY id DESC;";
    
    private final String recuperarCorridasRecebidasQuery = "SELECT * FROM taxi.corrida WHERE taxista = ? ORDER BY id DESC;";
    
    private final String updateCorridaQuery = "UPDATE taxi.corrida SET taxista = ?, cliente = ?, veiculo = ?, valorkm = ?, distancia = ?, localizacao_A_latitude = ?, localizacao_A_longitude = ?, localizacao_B_latitude = ?, localizacao_B_longitude = ?, qualificacao_motorista = ?, estrelas_motorista = ?, qualificacao_cliente = ?, estrelasCliente = ?, status = ?, datachamada = ? WHERE id = ?;";
    
    private final String rotasProximasDoPontoQuery = "SELECT * FROM taxi.ponto WHERE SQRT(POWER(latitude-?,2) + POWER(longitude-?,2)) < 0.0005;";
    
    private final String quantidadeRotasProximasdoPontoQuery = "SELECT COUNT(*) AS quantidade FROM taxi.ponto WHERE SQRT(POWER(latitude-?,2) + POWER(longitude-?,2)) < 0.0005 ";
           
    
    private final String recuperarRotaQuery = "SELECT * FROM taxi.ponto WHERE tempo >= (timestamp '?' - interval '3 minute') AND tempo <= (timestamp '?' + interval '3 minute') AND carro_id = ?";
    
    private final String primeiroPontoRotaQuery = "SELECT id AS inicio FROM taxi.ponto WHERE tempo >= (timestamp ? - interval '3 minute') AND tempo <= (timestamp ? + interval '3 minute') AND carro_id = ? ORDER BY tempo ASC LIMIT 1";
    
    
    public CorridaDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public int create(Corrida corrida){
        try {
            PreparedStatement statement = connection.prepareStatement(createCorrida);
            statement.setInt(1, corrida.getTaxista());
            statement.setInt(2, corrida.getCliente());
            statement.setInt(3, corrida.getVeiculo());
            statement.setFloat(4, corrida.getValorkm());
            statement.setFloat(5, corrida.getDistancia());
            
            statement.setString(6, corrida.getLocalizacao_A_latitude());
            statement.setString(7, corrida.getLocalizacao_A_longitude());
            statement.setString(8, corrida.getLocalizacao_B_latitude());
            statement.setString(9, corrida.getLocalizacao_B_longitude());
            statement.setString(10, corrida.getQualificacao_motorista());
            statement.setInt(11, corrida.getEstrelas_motorista());
            statement.setString(12, corrida.getQualificacao_cliente());
            statement.setInt(13, corrida.getEstrelas_cliente());
            statement.setInt(14, corrida.getStatus());
            statement.setInt(15, corrida.getPontorota());
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                corrida.setId(result.getInt("id"));
            }
            return corrida.getId();
        } catch (SQLException ex) {
            Logger.getLogger(CorridaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    @Override
    public Corrida read(int id){
         try {
            PreparedStatement statement = connection.prepareStatement(readCorrida);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            
            if (result.next()) {
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                corrida.setDataChamada(result.getTimestamp("datachamada"));
                corrida.setPontorota(result.getInt("pontorota"));
            }
            return corrida;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    @Override
    public void update(Corrida corrida){
        try {
            PreparedStatement statement = connection.prepareStatement(updateCorridaQuery);
            statement.setInt(1,corrida.getTaxista());
            statement.setInt(2,corrida.getCliente());
            statement.setInt(3,corrida.getVeiculo());
            statement.setFloat(4,corrida.getValorkm());
            statement.setFloat(5,corrida.getDistancia());
            statement.setString(6,corrida.getLocalizacao_A_latitude());
            statement.setString(7,corrida.getLocalizacao_A_longitude());
            statement.setString(8,corrida.getLocalizacao_B_latitude());
            statement.setString(9,corrida.getLocalizacao_B_longitude());
            statement.setString(10,corrida.getQualificacao_motorista());
            statement.setInt(11,corrida.getEstrelas_motorista());
            statement.setString(12,corrida.getQualificacao_cliente());
            statement.setInt(13,corrida.getEstrelas_cliente());
            statement.setInt(14,corrida.getStatus());
            statement.setTimestamp(15, corrida.getDataChamada());
            statement.setInt(16, corrida.getId());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void delete(int id){
    
    }
            
            
    @Override
    public List<Corrida> all(){
        List listaCorrida = null;
        return listaCorrida;
    }
    
    public List<Corrida> recuperarCorridasSolicidatas(int clienteId){
        try {
            PreparedStatement statement = connection.prepareStatement(recuperarCorridasSolicidatasQuery);
            statement.setInt(1, clienteId);
            ResultSet result = statement.executeQuery();
            List<Corrida> corridas = new ArrayList<Corrida>();
            
            Corrida corrida;
            
            while (result.next()) {
                corrida = new Corrida();
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                
                corridas.add(corrida);
            }
            return corridas;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Corrida> recuperarCorridasRecebidas(int motoristaId){
        try {
            PreparedStatement statement = connection.prepareStatement(recuperarCorridasRecebidasQuery);
            statement.setInt(1, motoristaId);
            ResultSet result = statement.executeQuery();
            List<Corrida> corridas = new ArrayList<Corrida>();
            
            Corrida corrida;
            
            while (result.next()) {
                corrida = new Corrida();
                corrida.setId(result.getInt("id"));
                corrida.setTaxista(result.getInt("taxista"));
                corrida.setCliente(result.getInt("cliente"));
                corrida.setVeiculo(result.getInt("veiculo"));     
                corrida.setValorkm(result.getFloat("valorkm")); 
                corrida.setDistancia(result.getFloat("distancia")); 
                corrida.setLocalizacao_A_latitude(result.getString("localizacao_a_latitude")); 
                corrida.setLocalizacao_A_longitude(result.getString("localizacao_a_longitude"));
                corrida.setLocalizacao_B_latitude(result.getString("localizacao_b_latitude"));
                corrida.setLocalizacao_B_longitude(result.getString("localizacao_b_longitude"));
                corrida.setQualificacao_motorista(result.getString("qualificacao_motorista"));
                corrida.setEstrelas_motorista(result.getInt("estrelas_motorista"));
                corrida.setQualificacao_cliente(result.getString("qualificacao_cliente"));
                corrida.setEstrelas_cliente(result.getInt("estrelasCliente"));
                corrida.setStatus(result.getInt("status"));
                
                corridas.add(corrida);
            }
            return corridas;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    public List<Ponto> recuperarRota(float longitude, float latitude){
        try {
            PreparedStatement statement = connection.prepareStatement(rotasProximasDoPontoQuery);
            statement.setFloat(1, latitude);
            statement.setFloat(2, longitude);
            ResultSet result = statement.executeQuery();
        
            List<Ponto> rota = new ArrayList<Ponto>();
            Ponto ponto;
            while (result.next()) {
                ponto = new Ponto();
                ponto.setId(result.getInt("id"));
                ponto.setCarro_id(result.getInt("carro_id"));
                ponto.setTempo(result.getTimestamp("tempo"));
                ponto.setLatitude(result.getFloat("latitude"));
                ponto.setLongitude(result.getFloat("longitude"));
                
                rota.add(ponto);
            }
            return rota;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int quantidadeRotasProximasdoPonto(float latitude, float longitude){
        try{
            PreparedStatement statement = connection.prepareStatement(quantidadeRotasProximasdoPontoQuery);
            statement.setFloat(1, latitude);
            statement.setFloat(2, longitude);
            ResultSet result = statement.executeQuery();
            result.next();
            int resultado = result.getInt("quantidade");
            return resultado;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    
    
    //rotasProximasDoPontoQuery
    
    
}


