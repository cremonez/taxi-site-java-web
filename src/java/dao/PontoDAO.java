package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import model.Ponto;


public class PontoDAO extends DAO<Ponto>{

    private final String readQuery = "SELECT * FROM taxi.ponto WHERE id = ?;";
    private final String rotaQuery = "SELECT * FROM taxi.ponto WHERE carro_id= ? AND tempo < '2008-02-02 18:34:16.0' AND tempo > (timestamp '2008-02-02 18:34:16.0' - interval '5 minute');";
    private final String rotaQuery2 = "SELECT * FROM taxi.ponto WHERE carro_id= ? AND tempo <= ? AND tempo >= ?;";
    
    
    public PontoDAO(Connection connection) {
        super(connection);
    }

    @Override
    public int create(Ponto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Ponto read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            Ponto ponto = new Ponto();
            if(result.next()) {
                ponto.setId(result.getInt("id"));
                ponto.setCarro_id(result.getInt("carro_id"));
                ponto.setLatitude(result.getFloat("latitude"));
                ponto.setLongitude(result.getFloat("longitude"));     
                ponto.setTempo(result.getTimestamp("tempo"));
            }
            return ponto;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void update(Ponto obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Ponto> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Ponto> getRota(int carro_id, Timestamp data_ponto, Timestamp data_chamada, boolean status) {
        try {
            PreparedStatement statement = connection.prepareStatement(rotaQuery2);
            statement.setInt(1, carro_id);
            statement.setTimestamp(2, data_ponto);
            // Tempo para o taxi chegar 10 min
            long tempoFaltando;
            if(status == false){
                tempoFaltando = (10 * 60 * 1000);
                if(tempoFaltando < 0){
                    tempoFaltando = 0;
                }
            }else{
                tempoFaltando = (10 * 60 * 1000) - (System.currentTimeMillis() - data_chamada.getTime());
                if(tempoFaltando < 0){
                    tempoFaltando = 0;
                }
            }
            
            //data_atual.setTime(data_atual.getTime() - 1000 * 60 * 5);
            data_ponto.setTime(data_ponto.getTime() - tempoFaltando );
            statement.setTimestamp(3, data_ponto);
           
            
            ResultSet result = statement.executeQuery();
            
            List<Ponto> rota = new ArrayList<Ponto>();
            
            Ponto ponto;
            
            while (result.next()) {
                ponto = new Ponto();
                
                ponto.setId(result.getInt("id"));
                ponto.setCarro_id(result.getInt("carro_id"));
                ponto.setLatitude(result.getFloat("latitude"));
                ponto.setLongitude(result.getFloat("longitude"));     
                ponto.setTempo(result.getTimestamp("tempo"));
                rota.add(ponto);
            }
            return rota;   
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   
}