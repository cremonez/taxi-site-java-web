CREATE SCHEMA TAXI;

CREATE TABLE TAXI.veiculo(
	id SERIAL PRIMARY KEY,
	marca VARCHAR(50),
	modelo VARCHAR(75),
	cor VARCHAR(25),
	capacidade INT,
	renavam VARCHAR(25),
	placa VARCHAR(10),
	ano INT,
	fotografia VARCHAR(512) DEFAULT 'default.png'
);

CREATE TABLE TAXI.usuario(
	id SERIAL PRIMARY KEY,
	email VARCHAR(150),
	senha VARCHAR(150),
	nome VARCHAR(50),
	sobrenome VARCHAR(100),
	nascimento DATE,
	telefone VARCHAR(15),
	sexo BOOLEAN,
	cpf VARCHAR(20),
	status INT,
	valorkm FLOAT,
	veiculo INT REFERENCES taxi.veiculo,
	fotografia VARCHAR(512) DEFAULT 'default.png'
);

CREATE TABLE TAXI.corrida(
	id SERIAL PRIMARY KEY,
	taxista INT REFERENCES taxi.usuario,
	cliente INT REFERENCES taxi.usuario,
	veiculo INT REFERENCES taxi.veiculo,
	valorkm FLOAT,
	distancia FLOAT,
	dataChamada TIMESTAMP DEFAULT now(),
	pontoRota INT REFERENCES taxi.ponto,
	localizacao_A_latitude float,
	localizacao_A_longitude float,
	localizacao_B_latitude float,
	localizacao_B_longitude float,
	qualificacao_motorista VARCHAR(1000),
	estrelas_motorista INT,
	qualificacao_cliente VARCHAR(1000),
	estrelascliente INT,
	status INT
);
ALTER TABLE taxi.corrida ADD COLUMN id_atual INT REFERENCES taxi.ponto


CREATE TABLE TAXI.ponto(
	id SERIAL,
	carro_id INT,
	tempo TIMESTAMP,
	longitude FLOAT,
	latitude FLOAT	
);
ALTER TABLE taxi.ponto ADD CONSTRAINT pk_ponto PRIMARY KEY (id);

